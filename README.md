# README #

This README talks about how to run energyconsumption application.

### How do I get set up? ###

* Download source code.
* Go to the root project folder and build it with Maven and Java 8.
* Run the service with following command from application root folder **java -jar target/energyconsumption-1.0-SNAPSHOT.jar**.
* Browse api documentation on http://localhost:8080/energyconsumption/swagger-ui.html.
* Use sample requests by importing Postman collection from **./postman** directory.
* Drop input data csv files in root project folder to have them processed by the service (you can find some input data in **./src/test/resources/testdata** directory).

### Who do I talk to? ###

* Jovan Kricka (jovankricka@gmail.com)