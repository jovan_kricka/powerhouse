package com.powerhouse.app;

import com.google.common.collect.Iterables;
import com.powerhouse.entities.MeterReading;
import com.powerhouse.helpers.ConnectionRestHelper;
import com.powerhouse.helpers.MeterReadingRestHelper;
import com.powerhouse.helpers.ProfileRestHelper;
import com.powerhouse.models.ConnectionModel;
import com.powerhouse.models.MeterReadingModel;
import com.powerhouse.models.MeterReadingSetModel;
import com.powerhouse.models.ProfileModel;
import com.powerhouse.util.Month;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * Tests for {@link MeterReading} endpoint.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MeterReadingRestEndpointTest extends BaseRestEndpointTest
{

    @LocalServerPort
    private int port;
    @Resource
    private ConnectionRestHelper connectionRestHelper;
    @Resource
    private ProfileRestHelper profileRestHelper;
    @Resource
    private MeterReadingRestHelper meterReadingRestHelper;

    @Before
    public void before()
    {
        connectionRestHelper.setPort(port);
        profileRestHelper.setPort(port);
        meterReadingRestHelper.setPort(port);
    }

    @Test
    public void shouldCreateMeterReadingsAndFetchThemOneByOne()
    {
        MeterReadingRestHelper.Prerequisites prerequisites = createPrerequisites();

        List<MeterReadingModel> retrievedMeterReadings = meterReadingRestHelper.getMeterReadings();
        for (MeterReadingModel expectedMeterReadingModel : retrievedMeterReadings)
        {
            MeterReadingModel actualMeterReading =
                    meterReadingRestHelper.getMeterReading(expectedMeterReadingModel.getId());
            meterReadingRestHelper.assertEqualMeterReading(expectedMeterReadingModel, actualMeterReading);
        }

        deletePrerequisites(prerequisites);
    }

    @Test
    public void shouldCreateSetOfMeterReadingsAndModifyOneOfThem()
    {
        MeterReadingRestHelper.Prerequisites prerequisites = createPrerequisites();

        List<MeterReadingModel> retrievedMeterReadings = meterReadingRestHelper.getMeterReadings();
        MeterReadingModel retrievedMeterReadingModel = Iterables.getLast(retrievedMeterReadings);
        float updatedValue = Float.parseFloat(retrievedMeterReadingModel.getValue()) + 0.0001f;
        MeterReadingModel updatedMeterReadingModel = retrievedMeterReadingModel.builderFromCurrent()
                .withValue(Float.toString(updatedValue))
                .build();
        meterReadingRestHelper.putMeterReading(updatedMeterReadingModel, HttpStatus.OK);

        retrievedMeterReadingModel = meterReadingRestHelper.getMeterReading(updatedMeterReadingModel.getId());
        meterReadingRestHelper.assertEqualMeterReading(updatedMeterReadingModel, retrievedMeterReadingModel);

        deletePrerequisites(prerequisites);
    }

    @Test
    public void shouldTryToUpdateNonExistingMeterReading()
    {
        MeterReadingRestHelper.Prerequisites prerequisites = createPrerequisites();

        MeterReadingModel nonExistingMeterReading = MeterReadingModel.builder()
                .withId("42")
                .withConnectionId(prerequisites.getConnectionId())
                .withProfileName(prerequisites.getProfileName())
                .withMonth(Month.JAN.toString())
                .withValue("0.1")
                .build();
        meterReadingRestHelper.putMeterReading(nonExistingMeterReading, HttpStatus.NOT_FOUND);

        deletePrerequisites(prerequisites);
    }

    @Test
    public void shouldCreateSetOfMeterReadingsModifyOneWithDeviatingValueAndGetBadRequest()
    {
        MeterReadingRestHelper.Prerequisites prerequisites = createPrerequisites();

        List<MeterReadingModel> retrievedMeterReadings = meterReadingRestHelper.getMeterReadings();
        MeterReadingModel retrievedMeterReadingModel = Iterables.getLast(retrievedMeterReadings);
        MeterReadingModel updatedMeterReadingModel = retrievedMeterReadingModel.builderFromCurrent()
                .withValue("1.5")
                .build();
        meterReadingRestHelper.putMeterReading(updatedMeterReadingModel, HttpStatus.BAD_REQUEST);

        deletePrerequisites(prerequisites);
    }

    @Test
    public void shouldTryToFetchNonExistingMeterReadingAndGetNotFound()
    {
        MeterReadingRestHelper.Prerequisites prerequisites = createPrerequisites();

        meterReadingRestHelper.givenMeterReadingEndpoint()
                .when().get("/42")
                .then().statusCode(HttpStatus.NOT_FOUND.value()).extract().response();

        deletePrerequisites(prerequisites);
    }

    /**
     * Creates prerequisites needed for testing meter readings.
     *
     * @return {@link com.powerhouse.helpers.MeterReadingRestHelper.Prerequisites}
     */
    private MeterReadingRestHelper.Prerequisites createPrerequisites()
    {
        ConnectionModel connectionModel = connectionRestHelper.buildConnectionModel("Amsterdam household");
        String connectionId = connectionRestHelper.createConnection(connectionModel);

        String profileName = "A";
        ProfileModel profileModel = profileRestHelper.buildValidProfileModel(profileName);
        String profileId = profileRestHelper.createProfile(profileModel);

        MeterReadingRestHelper.Prerequisites meterReadingsPrerequisites = new MeterReadingRestHelper.Prerequisites
                (connectionId, profileId, profileName);
        MeterReadingSetModel meterReadingSetModel = meterReadingRestHelper
                .buildMeterReadingSetModel(meterReadingsPrerequisites);
        meterReadingRestHelper.putMeterReadingSet(meterReadingSetModel, HttpStatus.OK);
        return meterReadingsPrerequisites;
    }

    /**
     * Deletes connection, profile and meter readings needed for testing meter readings.
     *
     * @param prerequisites {@link com.powerhouse.helpers.MeterReadingRestHelper.Prerequisites}
     */
    private void deletePrerequisites(MeterReadingRestHelper.Prerequisites prerequisites)
    {
        meterReadingRestHelper.deleteMeterReadingSet(prerequisites.getConnectionId());
        connectionRestHelper.deleteConnection(prerequisites.getConnectionId());
        profileRestHelper.deleteProfile(prerequisites.getProfileId());
    }

}
