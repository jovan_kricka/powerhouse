package com.powerhouse.app;

import com.jayway.restassured.RestAssured;
import org.junit.Before;

/**
 * Base class for REST tests.
 */
public class BaseRestEndpointTest
{

    @Before
    public void baseBefore()
    {
        RestAssured.baseURI = "http://localhost/energyconsumption";
    }

}
