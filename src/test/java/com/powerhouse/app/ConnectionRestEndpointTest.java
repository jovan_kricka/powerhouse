package com.powerhouse.app;

import com.powerhouse.entities.Connection;
import com.powerhouse.helpers.ConnectionRestHelper;
import com.powerhouse.models.ConnectionModel;
import org.apache.http.entity.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link Connection} REST endpoint.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ConnectionRestEndpointTest extends BaseRestEndpointTest
{

    @LocalServerPort
    private int port;
    @Resource
    private ConnectionRestHelper connectionRestHelper;

    @Before
    public void before()
    {
        connectionRestHelper.setPort(port);
    }

    @Test
    public void shouldCreateConnectionFetchAndDeleteIt()
    {
        ConnectionModel connectionModel = connectionRestHelper.buildConnectionModel("House");
        String id = connectionRestHelper.createConnection(connectionModel);
        connectionModel = connectionModel.builderFromCurrent().withId(id).build();

        ConnectionModel retrievedConnectionModel = connectionRestHelper.getConnection(id);
        assertEquals(connectionModel, retrievedConnectionModel);

        connectionRestHelper.deleteConnection(id);
        connectionRestHelper.assertConnectionDoesNotExist(id);
    }

    @Test
    public void shouldCreateTwoConnectionsFetchAndDeleteThem()
    {
        ConnectionModel firstConnectionModel = connectionRestHelper.buildConnectionModel("House");
        String firstId = connectionRestHelper.createConnection(firstConnectionModel);
        firstConnectionModel = firstConnectionModel.builderFromCurrent().withId(firstId).build();
        ConnectionModel secondConnectionModel = connectionRestHelper.buildConnectionModel("House");
        String secondId = connectionRestHelper.createConnection(secondConnectionModel);
        secondConnectionModel = secondConnectionModel.builderFromCurrent().withId(secondId).build();

        List<ConnectionModel> retrievedConnectionModels = connectionRestHelper.getConnections();
        assertEquals(firstConnectionModel, retrievedConnectionModels.get(0));
        assertEquals(secondConnectionModel, retrievedConnectionModels.get(1));

        connectionRestHelper.deleteConnection(firstConnectionModel.getId());
        connectionRestHelper.assertConnectionDoesNotExist(firstConnectionModel.getId());
        connectionRestHelper.deleteConnection(secondConnectionModel.getId());
        connectionRestHelper.assertConnectionDoesNotExist(secondConnectionModel.getId());
    }

    @Test
    public void shouldPutEmptyConnectionRequestBodyAndGetBadRequest()
    {
        connectionRestHelper.given()
                .when().contentType(ContentType.APPLICATION_JSON.getMimeType()).put()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void shouldCreateConnectionUpdateItsNameAndDeleteIt()
    {
        ConnectionModel connectionModel = connectionRestHelper.buildConnectionModel("House");
        String id = connectionRestHelper.createConnection(connectionModel);
        connectionModel = connectionModel.builderFromCurrent().withId(id).build();

        ConnectionModel retrievedConnectionModel = connectionRestHelper.getConnection(id);
        assertEquals(connectionModel, retrievedConnectionModel);

        ConnectionModel updatedConnectionModel = connectionRestHelper.buildConnectionModel("Factory")
                .builderFromCurrent().withId(id).build();
        connectionRestHelper.updateConnection(updatedConnectionModel);

        retrievedConnectionModel = connectionRestHelper.getConnection(id);
        assertEquals(updatedConnectionModel, retrievedConnectionModel);

        connectionRestHelper.deleteConnection(id);
        connectionRestHelper.assertConnectionDoesNotExist(id);
    }

    @Test
    public void shouldTryToDeleteConnectionThatDoesNotExist()
    {
        connectionRestHelper.given()
                .when().delete("/42")
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }


}
