package com.powerhouse.app;

import com.powerhouse.helpers.ConnectionRestHelper;
import com.powerhouse.helpers.ConsumptionRestHelper;
import com.powerhouse.helpers.MeterReadingRestHelper;
import com.powerhouse.helpers.ProfileRestHelper;
import com.powerhouse.models.ConnectionModel;
import com.powerhouse.models.MeterReadingSetModel;
import com.powerhouse.models.ProfileModel;
import com.powerhouse.util.Month;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;

/**
 * Tests for consumption endpoint.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ConsumptionRestEndpointTest extends BaseRestEndpointTest
{

    @LocalServerPort
    private int port;
    @Resource
    private ConnectionRestHelper connectionRestHelper;
    @Resource
    private ProfileRestHelper profileRestHelper;
    @Resource
    private MeterReadingRestHelper meterReadingRestHelper;
    @Resource
    private ConsumptionRestHelper consumptionRestHelper;

    @Before
    public void before()
    {
        connectionRestHelper.setPort(port);
        profileRestHelper.setPort(port);
        meterReadingRestHelper.setPort(port);
        consumptionRestHelper.setPort(port);
    }

    @Test
    public void shouldGetConsumptionsForGivenConnectionAndMonth()
    {
        MeterReadingRestHelper.Prerequisites prerequisites = createPrerequisites();
        String connectionId = prerequisites.getConnectionId();

        String retrievedConsumption = consumptionRestHelper.getConsumption(connectionId, Month.JAN);
        assertEquals("0", retrievedConsumption);
        retrievedConsumption = consumptionRestHelper.getConsumption(connectionId, Month.FEB);
        assertEquals("0", retrievedConsumption);
        retrievedConsumption = consumptionRestHelper.getConsumption(connectionId, Month.MAR);
        assertEquals("0.1", retrievedConsumption);
        retrievedConsumption = consumptionRestHelper.getConsumption(connectionId, Month.APR);
        assertEquals("0.1", retrievedConsumption);
        retrievedConsumption = consumptionRestHelper.getConsumption(connectionId, Month.MAY);
        assertEquals("0.1", retrievedConsumption);
        retrievedConsumption = consumptionRestHelper.getConsumption(connectionId, Month.JUN);
        assertEquals("0.1", retrievedConsumption);
        retrievedConsumption = consumptionRestHelper.getConsumption(connectionId, Month.JUL);
        assertEquals("0.1", retrievedConsumption);
        retrievedConsumption = consumptionRestHelper.getConsumption(connectionId, Month.AUG);
        assertEquals("0.1", retrievedConsumption);
        retrievedConsumption = consumptionRestHelper.getConsumption(connectionId, Month.SEP);
        assertEquals("0.1", retrievedConsumption);
        retrievedConsumption = consumptionRestHelper.getConsumption(connectionId, Month.OCT);
        assertEquals("0.1", retrievedConsumption);
        retrievedConsumption = consumptionRestHelper.getConsumption(connectionId, Month.NOV);
        assertEquals("0.1", retrievedConsumption);
        retrievedConsumption = consumptionRestHelper.getConsumption(connectionId, Month.DEC);
        assertEquals("0.1", retrievedConsumption);

        deletePrerequisites(prerequisites);
    }

    @Test
    public void shouldSendIdOfNonExistingConnectionAndGetNotFound()
    {
        MeterReadingRestHelper.Prerequisites prerequisites = createPrerequisites();

        consumptionRestHelper.given()
                .when().get("/42/" + Month.JAN.toString())
                .then().statusCode(HttpStatus.NOT_FOUND.value()).extract().response();

        deletePrerequisites(prerequisites);
    }

    @Test
    public void shouldSendInvalidMonthAndGetBadRequest()
    {
        MeterReadingRestHelper.Prerequisites prerequisites = createPrerequisites();

        consumptionRestHelper.given()
                .when().get("/" + prerequisites.getConnectionId() + "/NOTAMONTH")
                .then().statusCode(HttpStatus.BAD_REQUEST.value()).extract().response();

        deletePrerequisites(prerequisites);
    }

    @Test
    public void shouldSendConsumptionRequestForConnectionWithNoMeterReadingsAndGetNotFound()
    {
        MeterReadingRestHelper.Prerequisites prerequisites = createPrerequisites();
        meterReadingRestHelper.deleteMeterReadingSet(prerequisites.getConnectionId());

        consumptionRestHelper.given()
                .when().get("/" + prerequisites.getConnectionId() + "/" + Month.JAN.toString())
                .then().statusCode(HttpStatus.NOT_FOUND.value()).extract().response();

        connectionRestHelper.deleteConnection(prerequisites.getConnectionId());
        profileRestHelper.deleteProfile(prerequisites.getProfileId());
    }

    /**
     * Creates prerequisites needed for testing consumption.
     *
     * @return {@link com.powerhouse.helpers.MeterReadingRestHelper.Prerequisites}
     */
    private MeterReadingRestHelper.Prerequisites createPrerequisites()
    {
        ConnectionModel connectionModel = connectionRestHelper.buildConnectionModel("Amsterdam household");
        String connectionId = connectionRestHelper.createConnection(connectionModel);

        String profileName = "A";
        ProfileModel profileModel = profileRestHelper.buildValidProfileModel(profileName);
        String profileId = profileRestHelper.createProfile(profileModel);

        MeterReadingRestHelper.Prerequisites meterReadingsPrerequisites = new MeterReadingRestHelper.Prerequisites
                (connectionId, profileId, profileName);
        MeterReadingSetModel meterReadingSetModel = meterReadingRestHelper
                .buildMeterReadingSetModel(meterReadingsPrerequisites);
        meterReadingRestHelper.putMeterReadingSet(meterReadingSetModel, HttpStatus.OK);
        return meterReadingsPrerequisites;
    }

    /**
     * Deletes connection, profile and meter readings needed for testing consumption.
     *
     * @param prerequisites {@link com.powerhouse.helpers.MeterReadingRestHelper.Prerequisites}
     */
    private void deletePrerequisites(MeterReadingRestHelper.Prerequisites prerequisites)
    {
        meterReadingRestHelper.deleteMeterReadingSet(prerequisites.getConnectionId());
        connectionRestHelper.deleteConnection(prerequisites.getConnectionId());
        profileRestHelper.deleteProfile(prerequisites.getProfileId());
    }
}
