package com.powerhouse.app;

import com.powerhouse.entities.Connection;
import com.powerhouse.entities.Profile;
import com.powerhouse.helpers.ConnectionRestHelper;
import com.powerhouse.helpers.MeterReadingRestHelper;
import com.powerhouse.helpers.ProfileRestHelper;
import com.powerhouse.models.ConnectionModel;
import com.powerhouse.models.MeterReadingModel;
import com.powerhouse.models.MeterReadingSetModel;
import com.powerhouse.models.ProfileModel;
import org.assertj.core.util.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests for meter reading set endpoint.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MeterReadingSetRestEndpointTest extends BaseRestEndpointTest
{

    @LocalServerPort
    private int port;
    @Resource
    private MeterReadingRestHelper meterReadingRestHelper;
    @Resource
    private ConnectionRestHelper connectionRestHelper;
    @Resource
    private ProfileRestHelper profileRestHelper;
    private MeterReadingRestHelper.Prerequisites prerequisites;

    @Before
    public void before()
    {
        meterReadingRestHelper.setPort(port);
        connectionRestHelper.setPort(port);
        profileRestHelper.setPort(port);

        prerequisites = createPrerequisites();
    }

    @After
    public void after()
    {
        deletePrerequisites();
    }


    @Test
    public void shouldCreateSetOfMeterReadingsFetchAndDeleteIt()
    {
        MeterReadingSetModel meterReadingSetModel = meterReadingRestHelper.buildMeterReadingSetModel(prerequisites);
        meterReadingRestHelper.putMeterReadingSet(meterReadingSetModel, HttpStatus.OK);

        List<MeterReadingModel> retrievedMeterReadings = meterReadingRestHelper.getMeterReadings();
        meterReadingRestHelper.assertEqualMeterReadings(meterReadingSetModel.getMeterReadings(), retrievedMeterReadings);

        meterReadingRestHelper.deleteMeterReadingSet(prerequisites.getConnectionId());
        retrievedMeterReadings = meterReadingRestHelper.getMeterReadings();
        assertTrue(retrievedMeterReadings.isEmpty());
    }

    @Test
    public void shouldTryToCreateSetsWithInvalidNumberOfMeterReadingAndGetBadRequest()
    {
        MeterReadingSetModel meterReadingSetModel = meterReadingRestHelper
                .buildSetWithInvalidNumberOfMeterReadings(prerequisites);
        meterReadingRestHelper.putMeterReadingSet(meterReadingSetModel, HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldTryToCreateSetsWithTwoMeterReadingsForSameMonthAndGetBadRequest()
    {
        MeterReadingSetModel meterReadingSetModel = meterReadingRestHelper
                .buildMeterReadingSetWithDuplicateMonths(prerequisites);
        meterReadingRestHelper.putMeterReadingSet(meterReadingSetModel, HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldTryToCreateMeterReadingsForNonExistingConnectionAndGetBadRequest()
    {
        MeterReadingRestHelper.Prerequisites invalidPrerequisites = new MeterReadingRestHelper.Prerequisites(
                "42",
                prerequisites.getProfileId(),
                prerequisites.getProfileName()
        );
        MeterReadingSetModel meterReadingSetModel = meterReadingRestHelper
                .buildMeterReadingSetModel(invalidPrerequisites);
        meterReadingRestHelper.putMeterReadingSet(meterReadingSetModel, HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldTryToCreateSetsWithMeterReadingWithInvalidValueAndGetBadRequest()
    {
        MeterReadingSetModel meterReadingSetModel = meterReadingRestHelper
                .buildMeterReadingSetWithInvalidValue(prerequisites);
        meterReadingRestHelper.putMeterReadingSet(meterReadingSetModel, HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldTryToCreateSetsWithMeterReadingWithDeviatingValueAndGetBadRequest()
    {
        MeterReadingSetModel meterReadingSetModel = meterReadingRestHelper
                .buildMeterReadingSetWithDeviatingValue(prerequisites);
        meterReadingRestHelper.putMeterReadingSet(meterReadingSetModel, HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldTryToDeleteNonExistingMeterReadingsForConnectionAndGetNotFound()
    {
        meterReadingRestHelper.givenMeterReadingSetEndpoint()
                .when().delete("/42")
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void shouldCreateSetOfMeterReadingsFetchUpdateAndDeleteThem()
    {
        MeterReadingSetModel meterReadingSetModel = meterReadingRestHelper.buildMeterReadingSetModel(prerequisites);
        meterReadingRestHelper.putMeterReadingSet(meterReadingSetModel, HttpStatus.OK);

        List<MeterReadingModel> retrievedMeterReadings = meterReadingRestHelper.getMeterReadings();
        meterReadingRestHelper.assertEqualMeterReadings(meterReadingSetModel.getMeterReadings(), retrievedMeterReadings);

        MeterReadingSetModel modifiedMeterReadingSetModel = slightlyModifyMeterReadings(retrievedMeterReadings);
        meterReadingRestHelper.putMeterReadingSet(modifiedMeterReadingSetModel, HttpStatus.OK);

        retrievedMeterReadings = meterReadingRestHelper.getMeterReadings();
        meterReadingRestHelper.assertEqualMeterReadings(modifiedMeterReadingSetModel.getMeterReadings(), retrievedMeterReadings);

        meterReadingRestHelper.deleteMeterReadingSet(prerequisites.getConnectionId());
        retrievedMeterReadings = meterReadingRestHelper.getMeterReadings();
        assertTrue(retrievedMeterReadings.isEmpty());
    }

    /**
     * Slightly modifies values of given list of meter readings.
     *
     * @param meterReadingModels list of {@link MeterReadingModel}s
     * @return {@link MeterReadingSetModel}
     */
    private MeterReadingSetModel slightlyModifyMeterReadings(List<MeterReadingModel> meterReadingModels)
    {
        List<MeterReadingModel> updatedMeterReadingModels = Lists.newArrayList();
        updatedMeterReadingModels.add(meterReadingModels.get(0));
        updatedMeterReadingModels.add(meterReadingModels.get(1));
        for (int i = 2; i < meterReadingModels.size(); i++)
        {
            MeterReadingModel meterReadingModel = meterReadingModels.get(i);
            Float modifiedValue = Float.parseFloat(meterReadingModel.getValue()) + 0.0001f;
            MeterReadingModel updatedMeterReadingModel = meterReadingModel.builderFromCurrent()
                    .withValue(Float.toString(modifiedValue))
                    .build();
            updatedMeterReadingModels.add(updatedMeterReadingModel);
        }
        return MeterReadingSetModel.builder()
                .withMeterReadings(updatedMeterReadingModels)
                .build();
    }

    /**
     * Creates {@link Connection} and {@link Profile} needed for testing meter readings.
     */
    private MeterReadingRestHelper.Prerequisites createPrerequisites()
    {
        ConnectionModel connectionModel = connectionRestHelper.buildConnectionModel("Amsterdam household");
        String connectionId = connectionRestHelper.createConnection(connectionModel);

        String profileName = "A";
        ProfileModel profileModel = profileRestHelper.buildValidProfileModel(profileName);
        String profileId = profileRestHelper.createProfile(profileModel);

        return new MeterReadingRestHelper.Prerequisites(connectionId, profileId, profileName);
    }

    /**
     * Deletes {@link Connection} and {@link Profile}.
     */
    private void deletePrerequisites()
    {
        connectionRestHelper.deleteConnection(prerequisites.getConnectionId());
        profileRestHelper.deleteProfile(prerequisites.getProfileId());
    }

}
