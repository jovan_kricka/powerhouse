package com.powerhouse.app;

import com.powerhouse.entities.Profile;
import com.powerhouse.helpers.ProfileRestHelper;
import com.powerhouse.models.ProfileModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * Tests for {@link Profile} endpoint.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProfileRestEndpointTest extends BaseRestEndpointTest
{

    @LocalServerPort
    private int port;
    @Resource
    private ProfileRestHelper profileRestHelper;

    @Before
    public void before()
    {
        profileRestHelper.setPort(port);
    }

    @Test
    public void shouldCreateProfileFetchAndDeleteIt()
    {
        ProfileModel profileModel = profileRestHelper.buildValidProfileModel("A");
        String id = profileRestHelper.createProfile(profileModel);
        profileModel = profileModel.builderFromCurrent().withId(id).build();

        ProfileModel retrievedProfileModel = profileRestHelper.getProfile(id);
        profileRestHelper.assertEqualProfiles(profileModel, retrievedProfileModel);

        profileRestHelper.deleteProfile(id);
        profileRestHelper.assertProfileDoesNotExist(id);
    }

    @Test
    public void shouldCreateTwoProfilesFetchAndDeleteThem()
    {
        ProfileModel firstProfileModel = profileRestHelper.buildValidProfileModel("A");
        String firstId = profileRestHelper.createProfile(firstProfileModel);
        firstProfileModel = firstProfileModel.builderFromCurrent().withId(firstId).build();
        ProfileModel secondProfileModel = profileRestHelper.buildValidProfileModel("B");
        String secondId = profileRestHelper.createProfile(secondProfileModel);
        secondProfileModel = secondProfileModel.builderFromCurrent().withId(secondId).build();

        List<ProfileModel> retrievedConnectionModels = profileRestHelper.getProfiles();
        profileRestHelper.assertEqualProfiles(firstProfileModel, retrievedConnectionModels.get(0));
        profileRestHelper.assertEqualProfiles(secondProfileModel, retrievedConnectionModels.get(1));

        profileRestHelper.deleteProfile(firstProfileModel.getId());
        profileRestHelper.assertProfileDoesNotExist(firstProfileModel.getId());
        profileRestHelper.deleteProfile(secondProfileModel.getId());
        profileRestHelper.assertProfileDoesNotExist(secondProfileModel.getId());
    }

    @Test
    public void shouldCreateProfileUpdateItsNameAndDeleteIt()
    {
        ProfileModel profileModel = profileRestHelper.buildValidProfileModel("A");
        String id = profileRestHelper.createProfile(profileModel);
        profileModel = profileModel.builderFromCurrent().withId(id).build();

        ProfileModel retrievedProfileModel = profileRestHelper.getProfile(id);
        profileRestHelper.assertEqualProfiles(profileModel, retrievedProfileModel);


        ProfileModel updatedProfileModel = profileModel.builderFromCurrent()
                .withId(id)
                .withName("B")
                .build();
        profileRestHelper.updateProfile(updatedProfileModel);
        retrievedProfileModel = profileRestHelper.getProfile(id);
        profileRestHelper.assertEqualProfiles(updatedProfileModel, retrievedProfileModel);

        profileRestHelper.deleteProfile(id);
        profileRestHelper.assertProfileDoesNotExist(id);
    }

    @Test
    public void shouldTryToDeleteProfileThatDoesNotExist()
    {
        profileRestHelper.given()
                .when().delete("/42")
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void shouldTryToSubmitProfileWithMissingMonthAndGetBadRequest()
    {
        ProfileModel profileModel = profileRestHelper.buildProfileModelWithMissingMonth();
        profileRestHelper.putProfile(profileModel, HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldTryToSubmitProfileWithMissingNameAndGetBadRequest()
    {
        ProfileModel profileModel = profileRestHelper.buildProfileModelWithMissingName();
        profileRestHelper.putProfile(profileModel, HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldTryToSubmitProfileWithNonUniqueNameAndGetBadRequest()
    {
        ProfileModel firstProfileModel = profileRestHelper.buildValidProfileModel("A");
        String id = profileRestHelper.createProfile(firstProfileModel);

        ProfileModel secondProfileModel = profileRestHelper.buildValidProfileModel("A");
        profileRestHelper.putProfile(secondProfileModel, HttpStatus.BAD_REQUEST);

        profileRestHelper.deleteProfile(id);
    }

    @Test
    public void shouldTryToSubmitProfileWithDuplicateMonthAndGetBadRequest()
    {
        ProfileModel profileModel = profileRestHelper.buildProfileModelWithDuplicateMonth();
        profileRestHelper.putProfile(profileModel, HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldTryToSubmitProfileWithInvalidFractionSumAndGetBadRequest()
    {
        ProfileModel profileModel = profileRestHelper.buildProfileModelWithInvalidFractionSum();
        profileRestHelper.putProfile(profileModel, HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldTryToSubmitProfileWithInvalidFractionValueAndGetBadRequest()
    {
        ProfileModel profileModel = profileRestHelper.buildProfileModelWithInvalidFractionValue();
        profileRestHelper.putProfile(profileModel, HttpStatus.BAD_REQUEST);
    }


}
