package com.powerhouse.helpers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.powerhouse.entities.Connection;
import com.powerhouse.models.ConnectionModel;
import org.apache.http.entity.ContentType;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Helper class for making calls to {@link Connection} endpoint.
 */
@Component
public class ConnectionRestHelper extends BaseRestHelper
{

    private static final String ENDPOINT_URL = "/connection/";

    /**
     * Asserts that connection with the givenSet id does not exist.
     *
     * @param id of the connection
     */
    public void assertConnectionDoesNotExist(String id)
    {
        given()
                .when().get("/" + id)
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    /**
     * Sends DELETE request to the connection endpoint and deletes connection with the givenSet id.
     *
     * @param id of the connection to delete
     */
    public void deleteConnection(String id)
    {
        given()
                .when().delete("/" + id)
                .then().statusCode(HttpStatus.OK.value());
    }

    /**
     * Sends GET request to the connection endpoint and retrieves connection. This method expects that connection
     * with givenSet id exists.
     *
     * @param id of the connection
     * @return {@link ConnectionModel}
     */
    public ConnectionModel getConnection(String id)
    {
        Response response = given()
                .when().get("/" + id)
                .then().statusCode(HttpStatus.OK.value()).extract().response();
        return new Gson().fromJson(response.asString(), ConnectionModel.class);
    }

    /**
     * Sends GET request to the connection endpoint and retrieves all connections.
     *
     * @return list of {@link ConnectionModel}s
     */
    public List<ConnectionModel> getConnections()
    {
        Response response = given()
                .when().get()
                .then().statusCode(HttpStatus.OK.value()).extract().response();
        List<ConnectionModel> connectionModels = fromJsonArrayToConnectionModelList(response.asString());
        return connectionModels;
    }

    /**
     * Converts given json array to list of connections.
     *
     * @param jsonArray to convert
     * @return list of {@link ConnectionModel}s
     */
    private List<ConnectionModel> fromJsonArrayToConnectionModelList(String jsonArray)
    {
        return new Gson().fromJson(jsonArray, new TypeToken<List<ConnectionModel>>()
        {
        }.getType());
    }

    /**
     * Sends PUT request to the connection endpoint with the givenSet {@link ConnectionModel} to create a connection.
     *
     * @param connectionModel model of the connection to create
     * @return id of the created connection
     */
    public String createConnection(ConnectionModel connectionModel)
    {
        return putConnection(connectionModel, HttpStatus.CREATED);
    }

    /**
     * Sends PUT request to the connection endpoint with the givenSet {@link ConnectionModel} to update a connection.
     *
     * @param connectionModel model of the connection to update
     * @return id of the updated connection
     */
    public String updateConnection(ConnectionModel connectionModel)
    {
        return putConnection(connectionModel, HttpStatus.OK);
    }

    /**
     * Sends PUT request to create/update givenSet connection, and expects givenSet http status.
     *
     * @param connectionModel {@link ConnectionModel}
     * @param status          {@link HttpStatus}
     * @return id of the created/updated connection
     */
    private String putConnection(ConnectionModel connectionModel, HttpStatus status)
    {
        String connectionModelJson = new Gson().toJson(connectionModel);
        Response response = given()
                .when().contentType(ContentType.APPLICATION_JSON.getMimeType()).body(connectionModelJson).put()
                .then().statusCode(status.value()).extract().response();
        return response.asString();
    }

    /**
     * Returns request specification for connection endpoint.
     *
     * @return {@link RequestSpecification}
     */
    public RequestSpecification given()
    {
        return RestAssured.given().port(port).basePath(ENDPOINT_URL);
    }

    /**
     * Builds dummy {@link ConnectionModel}.
     *
     * @return dummy connection model
     */
    public ConnectionModel buildConnectionModel(String name)
    {
        return ConnectionModel.builder()
                .withName(name)
                .build();
    }

}
