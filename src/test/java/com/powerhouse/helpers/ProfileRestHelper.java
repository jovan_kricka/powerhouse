package com.powerhouse.helpers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.powerhouse.entities.Profile;
import com.powerhouse.models.FractionModel;
import com.powerhouse.models.ProfileModel;
import com.powerhouse.util.Month;
import org.apache.http.entity.ContentType;
import org.assertj.core.util.Lists;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

/**
 * Helper class for making calls to {@link Profile} endpoint.
 */
@Component
public class ProfileRestHelper extends BaseRestHelper
{

    private static final String ENDPOINT_URL = "/profile/";

    /**
     * Asserts that both givenSet profiles are equal.
     *
     * @param expected {@link ProfileModel}
     * @param actual   {@link ProfileModel}
     */
    public void assertEqualProfiles(ProfileModel expected, ProfileModel actual)
    {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getFractions().size(), actual.getFractions().size());
        for (int i = 0; i < expected.getFractions().size(); i++)
        {
            assertEqualFractions(expected.getFractions().get(i), actual.getFractions().get(i));
        }
    }

    /**
     * Asserts that both givenSet fraction models are equal. It does not check their ids.
     *
     * @param expected {@link FractionModel}
     * @param actual   {@link FractionModel}
     */
    public void assertEqualFractions(FractionModel expected, FractionModel actual)
    {
        assertEquals(expected.getMonth(), actual.getMonth());
        assertEquals(expected.getValue(), actual.getValue());
    }

    /**
     * Asserts that profile with the givenSet id does not exist.
     *
     * @param id of the profile
     */
    public void assertProfileDoesNotExist(String id)
    {
        given()
                .when().get("/" + id)
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    /**
     * Sends DELETE request to the profile endpoint and deletes profile with the givenSet id.
     *
     * @param id of the profile to delete
     */
    public void deleteProfile(String id)
    {
        given()
                .when().delete("/" + id)
                .then().statusCode(HttpStatus.OK.value());
    }

    /**
     * Sends GET request to the profile endpoint and retrieves profile. This method expects that profile with givenSet
     * id exists.
     *
     * @param id of the profile
     * @return {@link ProfileModel}
     */
    public ProfileModel getProfile(String id)
    {
        Response response = given()
                .when().get("/" + id)
                .then().statusCode(HttpStatus.OK.value()).extract().response();
        return new Gson().fromJson(response.asString(), ProfileModel.class);
    }

    /**
     * Sends GET request to the profile endpoint and retrieves all profiles.
     *
     * @return list of {@link ProfileModel}s
     */
    public List<ProfileModel> getProfiles()
    {
        Response response = given()
                .when().get()
                .then().statusCode(HttpStatus.OK.value()).extract().response();
        List<ProfileModel> profileModels = fromJsonArrayToProfileModelList(response.asString());
        return profileModels;
    }

    /**
     * Converts givenSet json array to list of profile models.
     *
     * @param jsonArray to convert
     * @return list of {@link ProfileModel}s
     */
    private List<ProfileModel> fromJsonArrayToProfileModelList(String jsonArray)
    {
        return new Gson().fromJson(jsonArray, new TypeToken<List<ProfileModel>>()
        {
        }.getType());
    }

    /**
     * Sends PUT request to the profile endpoint with the givenSet {@link ProfileModel} to create a profile.
     *
     * @param profileModel model of the profile to create
     * @return id of the created profile
     */
    public String createProfile(ProfileModel profileModel)
    {
        return putProfile(profileModel, HttpStatus.CREATED);
    }

    /**
     * Sends PUT request to the profile endpoint with the givenSet {@link ProfileModel} to update a profile.
     *
     * @param profileModel model of the profile to update
     * @return id of the updated profile
     */
    public String updateProfile(ProfileModel profileModel)
    {
        return putProfile(profileModel, HttpStatus.OK);
    }

    /**
     * Sends PUT request to create/update givenSet connection, and expects givenSet http status.
     *
     * @param profileModel {@link ProfileModel}
     * @param status       {@link HttpStatus}
     * @return id of the created/updated connection
     */
    public String putProfile(ProfileModel profileModel, HttpStatus status)
    {
        String profileModelJson = new Gson().toJson(profileModel);
        Response response = given()
                .when().contentType(ContentType.APPLICATION_JSON.getMimeType()).body(profileModelJson).put()
                .then().statusCode(status.value()).extract().response();
        return response.asString();
    }

    /**
     * Returns request specification for profile endpoint.
     *
     * @return {@link RequestSpecification}
     */
    public RequestSpecification given()
    {
        return RestAssured.given().port(port).basePath(ENDPOINT_URL);
    }

    /**
     * Builds valid profile model with all 12 fractions with same ratio.
     *
     * @param name of the profile
     * @return {@link ProfileModel}
     */
    public ProfileModel buildValidProfileModel(String name)
    {
        List<FractionModel> fractions = Lists.newArrayList(
                buildFractionModel(Month.JAN.toString(), "0"),
                buildFractionModel(Month.FEB.toString(), "0"));

        for (int i = 2; i < 12; i++)
        {
            fractions.add(buildFractionModel(Month.values()[i].toString(), "0.1"));
        }
        return buildProfileModel(name, fractions);
    }

    /**
     * Builds invalid profile model with missing fraction for January.
     *
     * @return {@link ProfileModel}
     */
    public ProfileModel buildProfileModelWithMissingMonth()
    {
        List<FractionModel> fractions = Lists.newArrayList(
                buildFractionModel(Month.FEB.toString(), "0"));

        for (int i = 2; i < 12; i++)
        {
            fractions.add(buildFractionModel(Month.values()[i].toString(), "0.1"));
        }
        return buildProfileModel("A", fractions);
    }

    /**
     * Builds invalid profile model with missing name.
     *
     * @return {@link ProfileModel}
     */
    public ProfileModel buildProfileModelWithMissingName()
    {
        List<FractionModel> fractions = Lists.newArrayList(
                buildFractionModel(Month.JAN.toString(), "0"),
                buildFractionModel(Month.FEB.toString(), "0"));

        for (int i = 2; i < 12; i++)
        {
            fractions.add(buildFractionModel(Month.values()[i].toString(), "0.1"));
        }
        return buildProfileModel(null, fractions);
    }

    /**
     * Builds invalid profile model with two fractions for one month.
     *
     * @return {@link ProfileModel}
     */
    public ProfileModel buildProfileModelWithDuplicateMonth()
    {
        List<FractionModel> fractions = Lists.newArrayList(
                buildFractionModel(Month.JAN.toString(), "0"),
                buildFractionModel(Month.JAN.toString(), "0"));

        for (int i = 2; i < 12; i++)
        {
            fractions.add(buildFractionModel(Month.values()[i].toString(), "0.1"));
        }
        return buildProfileModel("A", fractions);
    }

    /**
     * Builds invalid profile model with invalid fraction sum.
     *
     * @return {@link ProfileModel}
     */
    public ProfileModel buildProfileModelWithInvalidFractionSum()
    {
        List<FractionModel> fractions = Lists.newArrayList(
                buildFractionModel(Month.JAN.toString(), "0.5"),
                buildFractionModel(Month.FEB.toString(), "0.5"));

        for (int i = 2; i < 12; i++)
        {
            fractions.add(buildFractionModel(Month.values()[i].toString(), "0.1"));
        }
        return buildProfileModel("A", fractions);
    }

    /**
     * Builds invalid profile model with invalid fraction value.
     *
     * @return {@link ProfileModel}
     */
    public ProfileModel buildProfileModelWithInvalidFractionValue()
    {
        List<FractionModel> fractions = Lists.newArrayList(
                buildFractionModel(Month.JAN.toString(), "0.5"),
                buildFractionModel(Month.FEB.toString(), "bogus"));

        for (int i = 2; i < 12; i++)
        {
            fractions.add(buildFractionModel(Month.values()[i].toString(), "0.1"));
        }
        return buildProfileModel("A", fractions);
    }

    /**
     * Builds {@link ProfileModel}.
     *
     * @param name
     * @param fractions
     * @return profile model with givenSet fields set
     */
    private ProfileModel buildProfileModel(String name, List<FractionModel> fractions)
    {
        return ProfileModel.builder()
                .withName(name)
                .withFractions(fractions)
                .build();
    }

    /**
     * Builds {@link FractionModel}.
     *
     * @param month
     * @param value
     * @return fraction model with givenSet fields set
     */
    private FractionModel buildFractionModel(String month, String value)
    {
        return FractionModel.builder()
                .withMonth(month)
                .withValue(value)
                .build();
    }

}
