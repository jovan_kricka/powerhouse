package com.powerhouse.helpers;

/**
 * Base class for all REST call helpers.
 */
public class BaseRestHelper
{

    protected int port;

    /**
     * Sets port that will be used for requests.
     *
     * @param port
     */
    public void setPort(int port)
    {
        this.port = port;
    }

}
