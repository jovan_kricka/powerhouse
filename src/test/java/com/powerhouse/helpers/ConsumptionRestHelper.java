package com.powerhouse.helpers;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.powerhouse.util.Month;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * Helper for making consumption requests.
 */
@Component
public class ConsumptionRestHelper extends BaseRestHelper
{

    private static final String ENDPOINT_URL = "/consumption/";

    /**
     * Sends GET request to get the consumption for a connection with given id on a given month.
     *
     * @param connectionId id of the connection
     * @param month        {@link Month}
     * @return string representing consumption
     */
    public String getConsumption(String connectionId, Month month)
    {
        Response response = given()
                .when().get("/" + connectionId + "/" + month.toString())
                .then().statusCode(HttpStatus.OK.value()).extract().response();
        return response.asString();
    }

    /**
     * Returns request specification for consumption endpoint.
     *
     * @return {@link RequestSpecification}
     */
    public RequestSpecification given()
    {
        return RestAssured.given().port(port).basePath(ENDPOINT_URL);
    }

}
