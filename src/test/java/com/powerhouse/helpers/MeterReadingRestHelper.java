package com.powerhouse.helpers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.powerhouse.entities.MeterReading;
import com.powerhouse.models.MeterReadingModel;
import com.powerhouse.models.MeterReadingSetModel;
import com.powerhouse.util.Month;
import org.apache.http.entity.ContentType;
import org.assertj.core.util.Lists;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

/**
 * Helper class for making calls to {@link MeterReading} endpoint.
 */
@Component
public class MeterReadingRestHelper extends BaseRestHelper
{

    private static final String ENDPOINT_URL = "/meterReading/";
    private static final String SET_ENDPOINT_URL = "/meterReadingSet/";

    /**
     * Sends GET request to the meter reading endpoint and retrieves all meter readings.
     *
     * @return list of {@link MeterReadingModel}s
     */
    public List<MeterReadingModel> getMeterReadings()
    {
        Response response = givenMeterReadingEndpoint()
                .when().get()
                .then().statusCode(HttpStatus.OK.value()).extract().response();
        List<MeterReadingModel> connectionModels = fromJsonArrayToMeterReadingModelList(response.asString());
        return connectionModels;
    }

    /**
     * Sends GET request to the meter reading endpoint and retrieve meter reading with given id.
     *
     * @return {@link MeterReadingModel}
     */
    public MeterReadingModel getMeterReading(String id)
    {
        Response response = givenMeterReadingEndpoint()
                .when().get("/" + id)
                .then().statusCode(HttpStatus.OK.value()).extract().response();
        MeterReadingModel meterReadingModel = new Gson().fromJson(response.asString(), MeterReadingModel.class);
        return meterReadingModel;
    }

    /**
     * Converts given json array to list of meter readings.
     *
     * @param jsonArray to convert
     * @return list of {@link MeterReadingModel}s
     */
    private List<MeterReadingModel> fromJsonArrayToMeterReadingModelList(String jsonArray)
    {
        return new Gson().fromJson(jsonArray, new TypeToken<List<MeterReadingModel>>()
        {
        }.getType());
    }

    /**
     * Sends PUT request to create given set of meter readings.
     *
     * @param meterReadingSetModel {@link MeterReadingSetModel}
     */
    public void putMeterReadingSet(MeterReadingSetModel meterReadingSetModel, HttpStatus httpStatus)
    {
        String meterReadingSetModelJson = new Gson().toJson(meterReadingSetModel);
        givenMeterReadingSetEndpoint()
                .when().contentType(ContentType.APPLICATION_JSON.getMimeType()).body(meterReadingSetModelJson).put()
                .then().statusCode(httpStatus.value()).extract().response();
    }

    /**
     * Sends PUT request to update given meter reading.
     *
     * @param meterReadingModel {@link MeterReadingModel}
     */
    public void putMeterReading(MeterReadingModel meterReadingModel, HttpStatus httpStatus)
    {
        String meterReadingModelJson = new Gson().toJson(meterReadingModel, MeterReadingModel.class);
        givenMeterReadingEndpoint()
                .when().contentType(ContentType.APPLICATION_JSON.getMimeType()).body(meterReadingModelJson).put()
                .then().statusCode(httpStatus.value()).extract().response();
    }

    /**
     * Returns request specification for meter reading set endpoint.
     *
     * @return {@link RequestSpecification}
     */
    public RequestSpecification givenMeterReadingSetEndpoint()
    {
        return given().port(port).basePath(SET_ENDPOINT_URL);
    }

    /**
     * Returns request specification for meter reading endpoint.
     *
     * @return {@link RequestSpecification}
     */
    public RequestSpecification givenMeterReadingEndpoint()
    {
        return given().port(port).basePath(ENDPOINT_URL);
    }

    /**
     * Sends DELETE request to the meter reading set endpoint and deletes meter readings on connection with the givenSet
     * id.
     *
     * @param connectionId of the connection
     */
    public void deleteMeterReadingSet(String connectionId)
    {
        givenMeterReadingSetEndpoint()
                .when().delete("/" + connectionId)
                .then().statusCode(HttpStatus.OK.value());
    }

    /**
     * Simple helper class that wraps prerequisites for creating meter reading.
     */
    public static class Prerequisites
    {

        private String connectionId;
        private String profileId;
        private String profileName;

        public Prerequisites(String connectionId, String profileId, String profileName)
        {
            this.connectionId = connectionId;
            this.profileId = profileId;
            this.profileName = profileName;
        }

        public String getConnectionId()
        {
            return connectionId;
        }

        public String getProfileName()
        {
            return profileName;
        }

        public String getProfileId()
        {
            return profileId;
        }
    }

    /**
     * Builds set of meter readings with 12 {@link MeterReadingModel}s for one {@link Month} each.
     *
     * @param prerequisites wrapper containing connection id and profile name
     * @return {@link MeterReadingSetModel}
     */
    public MeterReadingSetModel buildMeterReadingSetModel(Prerequisites prerequisites)
    {
        List<MeterReadingModel> meterReadings = Lists.newArrayList(
                buildMeterReadingModel(prerequisites, Month.JAN, 0f),
                buildMeterReadingModel(prerequisites, Month.FEB, 0f)
        );

        for (int i = 1; i < 11; i++)
        {
            meterReadings.add(buildMeterReadingModel(prerequisites, Month.values()[i + 1], i * 0.1f));
        }

        return MeterReadingSetModel.builder()
                .withMeterReadings(meterReadings)
                .build();
    }

    /**
     * Builds set of meter readings with invalid number of {@link MeterReadingModel}s.
     *
     * @param prerequisites wrapper containing connection id and profile name
     * @return {@link MeterReadingSetModel}
     */
    public MeterReadingSetModel buildSetWithInvalidNumberOfMeterReadings(Prerequisites prerequisites)
    {
        MeterReadingSetModel validMeterReadingSetModel = buildMeterReadingSetModel(prerequisites);
        List<MeterReadingModel> invalidMeterReadings = validMeterReadingSetModel.getMeterReadings();
        invalidMeterReadings.remove(invalidMeterReadings.size() - 1);

        return MeterReadingSetModel.builder()
                .withMeterReadings(invalidMeterReadings)
                .build();
    }

    /**
     * Builds set of meter readings with two {@link MeterReadingModel}s for same month.
     *
     * @param prerequisites wrapper containing connection id and profile name
     * @return {@link MeterReadingSetModel}
     */
    public MeterReadingSetModel buildMeterReadingSetWithDuplicateMonths(Prerequisites prerequisites)
    {
        MeterReadingSetModel validMeterReadingSetModel = buildMeterReadingSetModel(prerequisites);
        List<MeterReadingModel> invalidMeterReadings = validMeterReadingSetModel.getMeterReadings();
        MeterReadingModel removedMeterReading = invalidMeterReadings.remove(invalidMeterReadings.size() - 1);
        invalidMeterReadings.add(
                removedMeterReading.builderFromCurrent()
                        .withMonth(Month.JAN.toString())
                        .build());

        return MeterReadingSetModel.builder()
                .withMeterReadings(invalidMeterReadings)
                .build();
    }

    /**
     * Builds set of meter readings with one {@link MeterReadingModel} with invalid value.
     *
     * @param prerequisites wrapper containing connection id and profile name
     * @return {@link MeterReadingSetModel}
     */
    public MeterReadingSetModel buildMeterReadingSetWithInvalidValue(Prerequisites prerequisites)
    {
        MeterReadingSetModel validMeterReadingSetModel = buildMeterReadingSetModel(prerequisites);
        List<MeterReadingModel> invalidMeterReadings = validMeterReadingSetModel.getMeterReadings();
        MeterReadingModel removedMeterReading = invalidMeterReadings.remove(invalidMeterReadings.size() - 1);
        invalidMeterReadings.add(
                removedMeterReading.builderFromCurrent()
                        .withValue("0.0")
                        .build());

        return MeterReadingSetModel.builder()
                .withMeterReadings(invalidMeterReadings)
                .build();
    }

    /**
     * Builds set of meter readings with one {@link MeterReadingModel} with value deviating from the profile by more
     * than allowed tolerance.
     *
     * @param prerequisites wrapper containing connection id and profile name
     * @return {@link MeterReadingSetModel}
     */
    public MeterReadingSetModel buildMeterReadingSetWithDeviatingValue(Prerequisites prerequisites)
    {
        MeterReadingSetModel validMeterReadingSetModel = buildMeterReadingSetModel(prerequisites);
        List<MeterReadingModel> invalidMeterReadings = validMeterReadingSetModel.getMeterReadings();
        MeterReadingModel removedMeterReading = invalidMeterReadings.remove(invalidMeterReadings.size() - 1);
        invalidMeterReadings.add(
                removedMeterReading.builderFromCurrent()
                        .withValue("2.5")
                        .build());

        return MeterReadingSetModel.builder()
                .withMeterReadings(invalidMeterReadings)
                .build();
    }

    /**
     * Builds meter reading model.
     *
     * @param prerequisites {@link Prerequisites}
     * @param month         {@link Month}
     * @param value         float
     * @return {@link MeterReadingModel}
     */
    private MeterReadingModel buildMeterReadingModel(Prerequisites prerequisites, Month month, float value)
    {
        return MeterReadingModel.builder()
                .withConnectionId(prerequisites.getConnectionId())
                .withProfileName(prerequisites.getProfileName())
                .withMonth(month.toString())
                .withValue(Float.toString(value))
                .build();
    }

    /**
     * Asserts that given meter reading lists have equal elements.
     *
     * @param expectedMeterReadings list of {@link MeterReadingModel}s
     * @param actualMeterReadings   list of {@link MeterReadingModel}s
     */
    public void assertEqualMeterReadings(List<MeterReadingModel> expectedMeterReadings,
                                         List<MeterReadingModel> actualMeterReadings)
    {
        assertEquals(expectedMeterReadings.size(), actualMeterReadings.size());

        for (int i = 0; i < expectedMeterReadings.size(); i++)
        {
            assertEqualMeterReading(expectedMeterReadings.get(i), actualMeterReadings.get(i));
        }
    }

    /**
     * Asserts that given meter readings have equal values.
     *
     * @param expected {@link MeterReadingModel}
     * @param actual   {@link MeterReadingModel}
     */
    public void assertEqualMeterReading(MeterReadingModel expected, MeterReadingModel actual)
    {
        assertEquals(expected.getConnectionId(), actual.getConnectionId());
        assertEquals(expected.getProfileName(), actual.getProfileName());
        assertEquals(expected.getMonth(), actual.getMonth());
        assertEquals(expected.getValue(), actual.getValue());
    }

}
