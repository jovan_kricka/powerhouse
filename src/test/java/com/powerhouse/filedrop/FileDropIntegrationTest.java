package com.powerhouse.filedrop;

import com.powerhouse.app.BaseRestEndpointTest;
import com.powerhouse.entities.Connection;
import com.powerhouse.helpers.ConnectionRestHelper;
import com.powerhouse.helpers.MeterReadingRestHelper;
import com.powerhouse.helpers.ProfileRestHelper;
import com.powerhouse.models.ConnectionModel;
import com.powerhouse.models.FractionModel;
import com.powerhouse.models.MeterReadingModel;
import com.powerhouse.models.ProfileModel;
import com.powerhouse.services.FileDropWatcherService;
import com.powerhouse.services.FileProcessingService;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Tests for file dropping functionality.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FileDropIntegrationTest extends BaseRestEndpointTest
{

    private static final String PROFILE_FRACTIONS_FILE = "./testdata/profileFractions.txt";
    private static final String METER_READINGS_FILE = "./testdata/meterReadings.txt";
    private static final String CONNECTION_ID_PLACEHOLDER = "<connection_id>";

    @LocalServerPort
    private int port;
    @Resource
    private ConnectionRestHelper connectionRestHelper;
    @Resource
    private ProfileRestHelper profileRestHelper;
    @Resource
    private MeterReadingRestHelper meterReadingRestHelper;

    @Before
    public void before()
    {
        connectionRestHelper.setPort(port);
        profileRestHelper.setPort(port);
        meterReadingRestHelper.setPort(port);
    }

    @Test
    public void shouldProcessProfileFractionsInputFile()
            throws IOException
    {
        copyFileToTheDropPath(PROFILE_FRACTIONS_FILE);
        ProfileModel processedProfile = waitUntilProfileFractionsAreProcessed();

        assertProfileWithTestData(processedProfile);
        profileRestHelper.deleteProfile(processedProfile.getId());
    }

    @Test
    public void shouldProcessMeterReadingsInputFile()
            throws IOException
    {
        copyFileToTheDropPath(PROFILE_FRACTIONS_FILE);
        ProfileModel processedProfile = waitUntilProfileFractionsAreProcessed();
        String connectionId = createConnection();
        replaceStringInFileAndCopyToDropPath(METER_READINGS_FILE, CONNECTION_ID_PLACEHOLDER, connectionId);
        List<MeterReadingModel> retrievedMeterReadings = meterReadingRestHelper.getMeterReadings();
        while (retrievedMeterReadings.isEmpty())
        {
            retrievedMeterReadings = meterReadingRestHelper.getMeterReadings();
        }

        assertMeterReadingsWithTestData(retrievedMeterReadings);
        meterReadingRestHelper.deleteMeterReadingSet(connectionId);
        profileRestHelper.deleteProfile(processedProfile.getId());
        connectionRestHelper.deleteConnection(connectionId);
    }

    /**
     * Waits until {@link FileDropWatcherService} picks up the file, and the {@link FileProcessingService} processes it.
     *
     * @return {@link ProfileModel} processed
     */
    public ProfileModel waitUntilProfileFractionsAreProcessed()
    {
        List<ProfileModel> retrievedProfiles = profileRestHelper.getProfiles();
        while (retrievedProfiles.isEmpty())
        {
            retrievedProfiles = profileRestHelper.getProfiles();
        }
        return retrievedProfiles.get(0);
    }

    /**
     * Asserts that given meter readings match test data.
     *
     * @param meterReadings list of {@link MeterReadingModel}s
     */
    private void assertMeterReadingsWithTestData(List<MeterReadingModel> meterReadings)
    {
        assertEquals("0.3", meterReadings.get(0).getValue());
        assertEquals("0.31", meterReadings.get(1).getValue());
        assertEquals("0.34", meterReadings.get(2).getValue());
        assertEquals("0.37", meterReadings.get(3).getValue());
        assertEquals("0.4", meterReadings.get(4).getValue());
        assertEquals("0.5", meterReadings.get(5).getValue());
        assertEquals("0.5", meterReadings.get(6).getValue());
        assertEquals("0.53", meterReadings.get(7).getValue());
        assertEquals("0.6", meterReadings.get(8).getValue());
        assertEquals("0.65", meterReadings.get(9).getValue());
        assertEquals("0.7", meterReadings.get(10).getValue());
        assertEquals("1.0", meterReadings.get(11).getValue());
    }

    /**
     * Asserts that given profile matches profile from the test data.
     *
     * @param profileModel {@link ProfileModel}
     */
    private void assertProfileWithTestData(ProfileModel profileModel)
    {
        assertEquals("A", profileModel.getName());
        List<FractionModel> fractions = profileModel.getFractions();

        assertEquals("0.3", fractions.get(0).getValue());
        assertEquals("0.01", fractions.get(1).getValue());
        assertEquals("0.03", fractions.get(2).getValue());
        assertEquals("0.03", fractions.get(3).getValue());
        assertEquals("0.03", fractions.get(4).getValue());
        assertEquals("0.1", fractions.get(5).getValue());
        assertEquals("0", fractions.get(6).getValue());
        assertEquals("0.03", fractions.get(7).getValue());
        assertEquals("0.07", fractions.get(8).getValue());
        assertEquals("0.05", fractions.get(9).getValue());
        assertEquals("0.05", fractions.get(10).getValue());
        assertEquals("0.3", fractions.get(11).getValue());
    }

    /**
     * Copies file from a given path to the file drop path where file will be processed by the application.
     *
     * @param filePath path of the file to copy
     * @return string representing destination file path
     */
    private String copyFileToTheDropPath(String filePath)
            throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();
        File inputFile = new File(classLoader.getResource(filePath).getFile());
        Path destinationPath = Paths.get(inputFile.getName());
        System.out.println("Copying " + inputFile.getAbsolutePath() + " to " + destinationPath.toAbsolutePath());
        FileUtils.copyFile(inputFile, destinationPath.toFile());
        return inputFile.getName();
    }

    /**
     * Creates {@link Connection} needed for testing file drop service.
     *
     * @return string representing connection id
     */
    private String createConnection()
    {
        ConnectionModel connectionModel = connectionRestHelper.buildConnectionModel("Amsterdam household");
        return connectionRestHelper.createConnection(connectionModel);
    }

    /**
     * Replaces given string with given replacement in a file on a given file path.
     *
     * @param filePath
     * @param stringToReplace
     * @param replacement
     * @throws IOException
     */
    private void replaceStringInFileAndCopyToDropPath(String filePath, String stringToReplace, String replacement)
            throws IOException
    {
        ClassLoader classLoader = getClass().getClassLoader();
        File inputFile = new File(classLoader.getResource(filePath).getFile());
        Charset charset = StandardCharsets.UTF_8;

        String content = new String(Files.readAllBytes(inputFile.toPath()), charset);
        content = content.replaceAll(stringToReplace, replacement);
        Path destinationPath = Paths.get(inputFile.getName());
        Files.write(destinationPath, content.getBytes(charset));
    }

}
