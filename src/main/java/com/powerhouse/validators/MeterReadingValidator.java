package com.powerhouse.validators;

import com.powerhouse.entities.MeterReading;
import com.powerhouse.util.Month;

import java.util.List;

/**
 * Validator for {@link MeterReading}s coming from the client side.
 */
public interface MeterReadingValidator
{

    /**
     * Validates {@link MeterReading} that is coming from the client side.
     *
     * @param meterReading    to validate
     * @param relatedReadings reading from previous {@link Month}
     * @return string representing error message if validation failed, or empty string otherwise
     */
    String validate(MeterReading meterReading, List<MeterReading> relatedReadings);

    /**
     * Validates list of 12 {@link MeterReading} that is coming from the client side.
     *
     * @param meterReadings to validate
     * @return string representing error message if validation failed, or empty string otherwise
     */
    String validate(List<MeterReading> meterReadings);

}
