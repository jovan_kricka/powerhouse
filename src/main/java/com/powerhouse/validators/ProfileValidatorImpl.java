package com.powerhouse.validators;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.powerhouse.entities.Fraction;
import com.powerhouse.entities.Profile;
import com.powerhouse.repositories.ProfileRepository;
import com.powerhouse.util.Month;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class ProfileValidatorImpl implements ProfileValidator
{

    private static final String WRONG_NUMBER_OF_FRACTIONS = "Profile must have exactly 12 valid fractions.";
    private static final String WRONG_FRACTION_SUM = "Sum of all 12 fractions must be equal exactly 1.";
    private static final String SAME_FRACTION_MULTIPLE_TIMES = "Fraction can appear only once in a profile.";
    private static final String SAME_MONTH_MULTIPLE_TIMES = "Profile must have fractions for all 12 months in a year.";
    private static final String MISSING_OR_NON_UNIQUE_PROFILE_NAME = "Profile name must exist and must be unique.";

    private final ProfileRepository profileRepository;

    @Autowired
    public ProfileValidatorImpl(ProfileRepository profileRepository)
    {
        this.profileRepository = profileRepository;
    }

    @Override
    public String validate(Profile profile)
    {
        if (hasWrongNumberOfFractions(profile))
        {
            return WRONG_NUMBER_OF_FRACTIONS;
        }
        if (isMissingOrNonUniqueProfileName(profile))
        {
            return MISSING_OR_NON_UNIQUE_PROFILE_NAME;
        }

        float fractionSum = 0;
        Set<String> fractionIds = Sets.newHashSet();
        Set<Month> months = Sets.newHashSet();
        for (Fraction fraction : profile.getFractions())
        {
            fractionSum += Math.round(fraction.getFraction() * 100.0f) / 100.0f;
            if (hasRepeatingFraction(fractionIds, fraction))
            {
                return SAME_FRACTION_MULTIPLE_TIMES;
            }
            if (hasRepeatingMonth(months, fraction))
            {
                return SAME_MONTH_MULTIPLE_TIMES;
            }
        }
        if ((Math.abs(fractionSum - 1.0f) > 0.001f))
        {
            return WRONG_FRACTION_SUM;
        }
        return "";
    }

    /**
     * Checks if given {@link Profile} has invalid number of {@link Fraction}s.
     *
     * @param profile to vaildate
     * @return true if number of fractions is invalid, false otherwise
     */
    private boolean hasWrongNumberOfFractions(Profile profile)
    {
        return profile.getFractions().size() != 12;
    }

    /**
     * Checks if given {@link Profile} is being created with missing or non-unique name.
     *
     * @param profile to validate
     * @return true if we are trying to create a profile with missing or non-unique name, false otherwise
     */
    private boolean isMissingOrNonUniqueProfileName(Profile profile)
    {
        return (profile.getId() == null || profileRepository.findOne(profile.getId()) == null) &&
                (profile.getName() == null || profileRepository.findByName(profile.getName()) != null);
    }

    /**
     * Checks if {@link Month} of given {@link Fraction} appears in the given set of months. If not, it adds that
     * month to the set.
     *
     * @param months   set of months to check against and update
     * @param fraction to validate
     * @return true if fraction has repeating month, false otherwise
     */
    private boolean hasRepeatingMonth(Set<Month> months, Fraction fraction)
    {
        if (months.contains(fraction.getMonth()))
        {
            return true;
        }
        months.add(fraction.getMonth());
        return false;
    }

    /**
     * Checks if id of given {@link Fraction} appears in the given map of ids. If not, it adds that id to the set.
     *
     * @param fractionIds set of ids to check against and update
     * @param fraction    to validate
     * @return true if fraction has repeating id, false otherwise
     */
    private boolean hasRepeatingFraction(Set<String> fractionIds, Fraction fraction)
    {
        if (Strings.isNullOrEmpty(fraction.getId()))
        {
            return false;
        }
        if (fractionIds.contains(fraction.getId()))
        {
            return true;
        }
        fractionIds.add(fraction.getId());
        return false;
    }

}
