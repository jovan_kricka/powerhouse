package com.powerhouse.validators;

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.powerhouse.entities.Connection;
import com.powerhouse.entities.MeterReading;
import com.powerhouse.entities.Profile;
import com.powerhouse.util.Month;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component
public class MeterReadingValidatorImpl implements MeterReadingValidator
{

    private static final double DEVIATION_TOLERANCE = 0.25;

    private static final String MISSING_CONNECTION_OR_PROFILE = "Connection or profile for a given meter reading is " +
            "not provided or it does not exists.";
    private static final String READING_LESS_THAN_PREVIOUS = "There is a meter reading that is less then meter " +
            "reading for a previous month.";
    private static final String METER_READING_DEVIATES_FROM_THE_PROFILE = "Meter reading deviates from the profile by" +
            " more than " + DEVIATION_TOLERANCE * 100 + "%.";
    private static final String INVALID_NUMBER_OF_METER_READINGS = "You must submit 12 meter readings exactly.";
    private static final String SAME_MONTH_MULTIPLE_TIMES = "There are multiple meter readings for the same month.";

    @Override
    public String validate(MeterReading meterReading, List<MeterReading> meterReadingsToUpdate)
    {
        if (meterReadingsToUpdate == null || meterReadingsToUpdate.size() != 12)
        {
            return INVALID_NUMBER_OF_METER_READINGS;
        }
        List<MeterReading> updatedMeterReadings = updateMeterReadings(meterReadingsToUpdate, meterReading);
        return validate(updatedMeterReadings);
    }

    @Override
    public String validate(List<MeterReading> meterReadings)
    {
        if (meterReadings == null || meterReadings.size() != 12)
        {
            return INVALID_NUMBER_OF_METER_READINGS;
        }
        if (hasRepeatingMonths(meterReadings))
        {
            return SAME_MONTH_MULTIPLE_TIMES;
        }
        if (hasMissingConnectionsOrProfiles(meterReadings))
        {
            return MISSING_CONNECTION_OR_PROFILE;
        }
        if (hasInvalidMeterReading(meterReadings))
        {
            return READING_LESS_THAN_PREVIOUS;
        }
        if (deviatesFromTheProfile(meterReadings))
        {
            return METER_READING_DEVIATES_FROM_THE_PROFILE;
        }
        return null;
    }

    /**
     * Checks if given {@link MeterReading}s have repeating {@link Month}s.
     *
     * @param meterReadings to validate
     * @return true if meter readings have repeating months, false otherwise
     */
    private boolean hasRepeatingMonths(List<MeterReading> meterReadings)
    {
        Set<Month> months = Sets.newHashSet();
        for (MeterReading meterReading : meterReadings)
        {
            if (months.contains(meterReading.getMonth()))
            {
                return true;
            }
            months.add(meterReading.getMonth());
        }
        return false;
    }

    /**
     * Updates given {@link MeterReading}s with given meter reading.
     *
     * @param meterReadings to update
     * @param meterReading
     * @return list of updated meter readings
     */
    private List<MeterReading> updateMeterReadings(List<MeterReading> meterReadings, MeterReading meterReading)
    {
        int indexToUpdate = 0;
        for (int i = 0; i < 12; i++)
        {
            if (meterReadings.get(i).getId().equals(meterReading.getId()))
            {
                indexToUpdate = i;
                break;
            }
        }
        meterReadings.remove(indexToUpdate);
        meterReadings.add(indexToUpdate, meterReading);
        return meterReadings;
    }

    /**
     * Checks if there are any {@link MeterReading}s with missing {@link Connection} or {@link Profile}.
     *
     * @param meterReadings to validate
     * @return true if there is meter reading with missing connection or profile, false otherwise
     */
    private boolean hasMissingConnectionsOrProfiles(List<MeterReading> meterReadings)
    {
        for (MeterReading meterReading : meterReadings)
        {
            if (meterReading.getConnection() == null || meterReading.getProfile() == null)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if provided {@link MeterReading}s adhere to their profile fractions. This method assumes that list of
     * meter readings has exactly 12 readings, and also assumes that for a given connection there are no meter
     * readings in the previous year (it does not take year into account at all).
     *
     * @param meterReadings to validate
     * @return true if any meter reading deviates from the profile, false otherwise
     */
    private boolean deviatesFromTheProfile(List<MeterReading> meterReadings)
    {
        double totalEnergySpent = Iterables.getLast(meterReadings).getValue();
        for (int i = 0; i < 12; i++)
        {
            MeterReading reading = meterReadings.get(i);
            Profile profile = reading.getProfile();

            double expectedConsumption = totalEnergySpent * profile.getFractions().get(i).getFraction();
            double tolerance = expectedConsumption * DEVIATION_TOLERANCE;
            double energySpent = i == 0 ? reading.getValue() : reading.getValue() - meterReadings.get(i - 1).getValue();

            if (energySpent < expectedConsumption - tolerance || energySpent > expectedConsumption + tolerance)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if among given {@link MeterReading}s there is some that has reading that is less than the previous
     * meter reading.
     *
     * @param meterReadings meter readings to validate
     * @return true if there is an invalid meter readings, false otherwise
     */
    private boolean hasInvalidMeterReading(List<MeterReading> meterReadings)
    {
        for (int i = 1; i < meterReadings.size(); i++)
        {
            if (meterReadings.get(i).getValue() < meterReadings.get(i - 1).getValue())
            {
                return true;
            }
        }
        return false;
    }

}
