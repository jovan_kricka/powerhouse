package com.powerhouse.validators;

import com.powerhouse.entities.Profile;

/**
 * Validates structure of {@link Profile} that is coming from the client side.
 */
public interface ProfileValidator
{

    /**
     * Validates {@link Profile} coming from the client side.
     *
     * @param profile to validate
     * @return string representing error message if validation failed, or empty string otherwise
     */
    String validate(Profile profile);

}
