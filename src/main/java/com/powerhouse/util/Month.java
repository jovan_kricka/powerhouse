package com.powerhouse.util;

/**
 * Enum representing months of the year.
 */
public enum Month
{
    JAN,
    FEB,
    MAR,
    APR,
    MAY,
    JUN,
    JUL,
    AUG,
    SEP,
    OCT,
    NOV,
    DEC;

    /**
     * Checks if given string represents a valid month abbreviation.
     *
     * @param monthCandidate {@link Month}
     * @return true if given string is valid month abbreviation, false otherwise
     */
    public static boolean isValidMonth(String monthCandidate)
    {
        try
        {
            Month.valueOf(monthCandidate);
            return true;
        } catch (IllegalArgumentException ex)
        {
            return false;
        }
    }

    /**
     * Gets previous {@link Month} for the current one. If current one is {@link Month#JAN} then null is returned.
     *
     * @return previous month or null if current month is JAN
     */
    public Month previous()
    {
        if (this.ordinal() == 0)
        {
            return null;
        }
        return Month.values()[this.ordinal() - 1];
    }

}
