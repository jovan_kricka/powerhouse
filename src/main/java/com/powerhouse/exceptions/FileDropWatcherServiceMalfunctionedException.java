package com.powerhouse.exceptions;

import com.powerhouse.services.FileDropWatcherService;

/**
 * Thrown when {@link FileDropWatcherService} malfunctions.
 */
public class FileDropWatcherServiceMalfunctionedException extends RuntimeException
{

    public FileDropWatcherServiceMalfunctionedException(Throwable cause)
    {
        super(cause);
    }

}
