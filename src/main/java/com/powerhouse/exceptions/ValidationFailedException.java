package com.powerhouse.exceptions;

/**
 * Thrown when entity validation fails.
 */
public class ValidationFailedException extends Throwable
{
    public ValidationFailedException(String validationError)
    {
        super(validationError);
    }
}
