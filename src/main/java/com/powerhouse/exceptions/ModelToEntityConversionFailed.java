package com.powerhouse.exceptions;

/**
 * Thrown when conversion from entity to model fails.
 */
public class ModelToEntityConversionFailed extends Throwable
{
    public ModelToEntityConversionFailed(Throwable cause)
    {
        super(cause.getMessage());
    }
}
