package com.powerhouse.adapters;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.powerhouse.entities.Fraction;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.models.FractionModel;
import com.powerhouse.repositories.ProfileRepository;
import com.powerhouse.util.Month;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.List;

@Component
public class FractionAdapterImpl implements FractionAdapter
{

    private static final String FRACTION_VALUE_CAN_NOT_BE_NULL = "Fraction value can not be null";
    private static final String FRACTION_MONTH_CAN_NOT_BE_NULL = "Fraction month can not be null";

    private final ProfileRepository profileRepository;

    @Autowired
    public FractionAdapterImpl(ProfileRepository profileRepository)
    {
        this.profileRepository = profileRepository;
    }

    @Override
    public Fraction toEntity(FractionModel model)
            throws ModelToEntityConversionFailed
    {
        try
        {
            checkNotNullableFields(model);

            float fraction = Float.parseFloat(model.getValue());
            Month month = Month.valueOf(model.getMonth());

            return Fraction.builder()
                    .withId(model.getId())
                    .withFraction(fraction)
                    .withMonth(month)
                    .build();
        } catch (IllegalArgumentException ex)
        {
            throw new ModelToEntityConversionFailed(ex);
        }
    }

    @Override
    public List<Fraction> toEntities(List<FractionModel> models)
            throws ModelToEntityConversionFailed
    {
        List<Fraction> entities = Lists.newArrayList();
        for (FractionModel model : models)
        {
            entities.add(toEntity(model));
        }
        return entities;
    }

    @Override
    public FractionModel toModel(Fraction entity)
    {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        return FractionModel.builder()
                .withId(entity.getId())
                .withValue(decimalFormat.format(entity.getFraction()))
                .withMonth(entity.getMonth().toString())
                .build();
    }

    @Override
    public List<FractionModel> toModels(List<Fraction> entities)
    {
        List<FractionModel> models = Lists.newArrayList();
        for (Fraction entity : entities)
        {
            models.add(toModel(entity));
        }
        return models;
    }

    /**
     * Checks if value and month fields of {@link FractionModel} are not null.
     *
     * @param model to check
     * @throws IllegalArgumentException if any of these fields is null
     */
    private void checkNotNullableFields(FractionModel model)
    {
        Preconditions.checkArgument(model.getValue() != null, FRACTION_VALUE_CAN_NOT_BE_NULL);
        Preconditions.checkArgument(model.getMonth() != null, FRACTION_MONTH_CAN_NOT_BE_NULL);
    }
}
