package com.powerhouse.adapters;

import com.powerhouse.entities.MeterReading;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.models.MeterReadingModel;

import java.util.List;

/**
 * Converts {@link MeterReadingModel}s to {@link MeterReading}s and vice versa.
 */
public interface MeterReadingAdapter
{

    /**
     * Converts given meter reading model to meter reading entity.
     *
     * @param model {@link MeterReadingModel} to convert
     * @return {@link MeterReading}
     * @throws ModelToEntityConversionFailed
     */
    MeterReading toEntity(MeterReadingModel model)
            throws ModelToEntityConversionFailed;

    /**
     * Converts given meter reading models to meter reading entities.
     *
     * @param models list of {@link MeterReadingModel}s to convert
     * @return list of {@link MeterReading}s
     * @throws ModelToEntityConversionFailed
     */
    List<MeterReading> toEntities(List<MeterReadingModel> models)
            throws ModelToEntityConversionFailed;

    /**
     * Converts given meter reading entity to meter reading model.
     *
     * @param entity {@link MeterReading} to convert
     * @return {@link MeterReadingModel}
     * @throws ModelToEntityConversionFailed
     */
    MeterReadingModel toModel(MeterReading entity);

    /**
     * Converts given meter reading entities to meter reading models.
     *
     * @param entities list of {@link MeterReading}s to convert
     * @return list of {@link MeterReadingModel}s
     * @throws ModelToEntityConversionFailed
     */
    List<MeterReadingModel> toModels(List<MeterReading> entities);

}
