package com.powerhouse.adapters;

import com.powerhouse.entities.Fraction;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.models.FractionModel;

import java.util.List;

/**
 * Provides api for converting {@link Fraction}s to {@link FractionModel}s and vice versa.
 */
public interface FractionAdapter
{

    /**
     * Converts given fraction model to fraction entity.
     *
     * @param model {@link FractionModel} to convert
     * @return {@link Fraction}
     * @throws ModelToEntityConversionFailed
     */
    Fraction toEntity(FractionModel model)
            throws ModelToEntityConversionFailed;

    /**
     * Converts given fraction models to fraction entities.
     *
     * @param models list of {@link FractionModel}s to convert
     * @return list of {@link Fraction}s
     * @throws ModelToEntityConversionFailed
     */
    List<Fraction> toEntities(List<FractionModel> models)
            throws ModelToEntityConversionFailed;

    /**
     * Converts given fraction entity to fraction model.
     *
     * @param entity {@link Fraction} to convert
     * @return {@link FractionModel}
     * @throws ModelToEntityConversionFailed
     */
    FractionModel toModel(Fraction entity);

    /**
     * Converts given fraction entities to fraction models.
     *
     * @param entities list of {@link Fraction}s to convert
     * @return list of {@link FractionModel}s
     * @throws ModelToEntityConversionFailed
     */
    List<FractionModel> toModels(List<Fraction> entities);

}
