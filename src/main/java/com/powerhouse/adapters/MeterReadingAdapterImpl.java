package com.powerhouse.adapters;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.powerhouse.entities.Connection;
import com.powerhouse.entities.MeterReading;
import com.powerhouse.entities.Profile;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.models.MeterReadingModel;
import com.powerhouse.repositories.ConnectionRepository;
import com.powerhouse.repositories.MeterReadingRepository;
import com.powerhouse.repositories.ProfileRepository;
import com.powerhouse.util.Month;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MeterReadingAdapterImpl implements MeterReadingAdapter
{

    private static final String READING_VALUE_CAN_NOT_BE_NULL = "Meter reading value can not be null.";
    private static final String READING_MONTH_CAN_NOT_BE_NULL = "Meter reading month can not be null.";
    private static final String READING_CONNECTION_CAN_NOT_BE_NULL = "Meter reading connection can not be null.";
    private static final String READING_PROFILE_CAN_NOT_BE_NULL = "Meter reading profile can not be null.";

    private final ConnectionRepository connectionRepository;
    private final ProfileRepository profileRepository;
    private final MeterReadingRepository meterReadingRepository;

    @Autowired
    public MeterReadingAdapterImpl(ConnectionRepository connectionRepository, ProfileRepository profileRepository,
                                   MeterReadingRepository meterReadingRepository)
    {
        this.connectionRepository = connectionRepository;
        this.profileRepository = profileRepository;
        this.meterReadingRepository = meterReadingRepository;
    }

    @Override
    public MeterReading toEntity(MeterReadingModel model)
            throws ModelToEntityConversionFailed
    {
        try
        {
            checkNonNullableFields(model);

            Connection connection = connectionRepository.findOne(model.getConnectionId());
            Profile profile = profileRepository.findByName(model.getProfileName());
            Month month = Month.valueOf(model.getMonth());
            Double reading = Double.parseDouble(model.getValue());
            MeterReading existingMeterReading = meterReadingRepository.findByConnectionAndMonth(connection, month);

            return MeterReading.builder()
                    .withId(existingMeterReading == null ? model.getId() : existingMeterReading.getId())
                    .withConnection(connection)
                    .withMonth(month)
                    .withProfile(profile)
                    .withValue(reading)
                    .build();
        } catch (IllegalArgumentException ex)
        {
            throw new ModelToEntityConversionFailed(ex);
        }
    }

    @Override
    public List<MeterReading> toEntities(List<MeterReadingModel> models)
            throws ModelToEntityConversionFailed
    {
        List<MeterReading> entities = Lists.newArrayList();
        for (MeterReadingModel model : models)
        {
            entities.add(toEntity(model));
        }
        return entities;
    }

    @Override
    public MeterReadingModel toModel(MeterReading entity)
    {
        return MeterReadingModel.builder()
                .withId(entity.getId())
                .withConnectionId(entity.getConnection().getId())
                .withMonth(entity.getMonth().toString())
                .withProfileName(entity.getProfile().getName())
                .withValue(Double.toString(entity.getValue()))
                .build();
    }

    @Override
    public List<MeterReadingModel> toModels(List<MeterReading> entities)
    {
        List<MeterReadingModel> models = Lists.newArrayList();
        for (MeterReading entity : entities)
        {
            models.add(toModel(entity));
        }
        return models;
    }

    /**
     * Checks if value, month, connectionId and profileId fields of {@link MeterReadingModel} are not null.
     *
     * @param model to check
     * @throws IllegalArgumentException if any of these fields is null
     */
    private void checkNonNullableFields(MeterReadingModel model)
    {
        Preconditions.checkArgument(model.getValue() != null, READING_VALUE_CAN_NOT_BE_NULL);
        Preconditions.checkArgument(model.getMonth() != null, READING_MONTH_CAN_NOT_BE_NULL);
        Preconditions.checkArgument(model.getConnectionId() != null, READING_CONNECTION_CAN_NOT_BE_NULL);
        Preconditions.checkArgument(model.getProfileName() != null, READING_PROFILE_CAN_NOT_BE_NULL);
    }
}
