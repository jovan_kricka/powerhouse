package com.powerhouse.adapters;

import com.google.common.collect.Lists;
import com.powerhouse.entities.Connection;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.models.ConnectionModel;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ConnectionAdapterImpl implements ConnectionAdapter
{
    @Override
    public Connection toEntity(ConnectionModel model)
            throws ModelToEntityConversionFailed
    {
        return Connection.builder()
                .withId(model.getId())
                .withName(model.getName())
                .build();
    }

    @Override
    public List<Connection> toEntities(List<ConnectionModel> models)
            throws ModelToEntityConversionFailed
    {
        List<Connection> entities = Lists.newArrayList();
        for (ConnectionModel model : models)
        {
            entities.add(toEntity(model));
        }
        return entities;
    }

    @Override
    public ConnectionModel toModel(Connection entity)
    {
        return ConnectionModel.builder()
                .withId(entity.getId())
                .withName(entity.getName())
                .build();
    }

    @Override
    public List<ConnectionModel> toModels(List<Connection> entities)
    {
        List<ConnectionModel> models = Lists.newArrayList();
        for (Connection entity : entities)
        {
            models.add(toModel(entity));
        }
        return models;
    }
}
