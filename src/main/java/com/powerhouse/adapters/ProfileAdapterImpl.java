package com.powerhouse.adapters;

import com.google.common.collect.Lists;
import com.powerhouse.entities.Profile;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.models.ProfileModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProfileAdapterImpl implements ProfileAdapter
{

    private final FractionAdapter fractionAdapter;

    @Autowired
    public ProfileAdapterImpl(FractionAdapter fractionAdapter)
    {
        this.fractionAdapter = fractionAdapter;
    }

    @Override
    public Profile toEntity(ProfileModel model)
            throws ModelToEntityConversionFailed
    {
        return Profile.builder()
                .withId(model.getId())
                .withFractions(fractionAdapter.toEntities(model.getFractions()))
                .withName(model.getName())
                .build();
    }

    @Override
    public List<Profile> toEntities(List<ProfileModel> models)
            throws ModelToEntityConversionFailed
    {
        List<Profile> entities = Lists.newArrayList();
        for (ProfileModel model : models)
        {
            entities.add(toEntity(model));
        }
        return entities;
    }

    @Override
    public ProfileModel toModel(Profile entity)
    {
        return ProfileModel.builder()
                .withId(entity.getId())
                .withFractions(fractionAdapter.toModels(entity.getFractions()))
                .withName(entity.getName())
                .build();
    }

    @Override
    public List<ProfileModel> toModels(List<Profile> entities)
    {
        List<ProfileModel> models = Lists.newArrayList();
        for (Profile entity : entities)
        {
            models.add(toModel(entity));
        }
        return models;
    }
}
