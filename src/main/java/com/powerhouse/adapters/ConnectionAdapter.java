package com.powerhouse.adapters;

import com.powerhouse.entities.Connection;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.models.ConnectionModel;

import java.util.List;

/**
 * Defines api for converting {@link ConnectionModel}s to {@link Connection}s and vice versa.
 */
public interface ConnectionAdapter
{

    /**
     * Converts given connection model to connection entity.
     *
     * @param model {@link ConnectionModel} to convert
     * @return {@link Connection}
     * @throws ModelToEntityConversionFailed
     */
    Connection toEntity(ConnectionModel model)
            throws ModelToEntityConversionFailed;

    /**
     * Converts given connection models to connection entities.
     *
     * @param models list of {@link ConnectionModel}s to convert
     * @return list of {@link Connection}s
     * @throws ModelToEntityConversionFailed
     */
    List<Connection> toEntities(List<ConnectionModel> models)
            throws ModelToEntityConversionFailed;

    /**
     * Converts given connection entity to connection model.
     *
     * @param entity {@link Connection} to convert
     * @return {@link ConnectionModel}
     * @throws ModelToEntityConversionFailed
     */
    ConnectionModel toModel(Connection entity);

    /**
     * Converts given connection entities to connection models.
     *
     * @param entities list of {@link Connection}s to convert
     * @return list of {@link ConnectionModel}s
     * @throws ModelToEntityConversionFailed
     */
    List<ConnectionModel> toModels(List<Connection> entities);

}
