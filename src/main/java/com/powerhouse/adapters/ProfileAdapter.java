package com.powerhouse.adapters;

import com.powerhouse.entities.Profile;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.models.ProfileModel;

import java.util.List;

/**
 * Provides api for converting {@link Profile}s to {@link ProfileModel}s and vice versa.
 */
public interface ProfileAdapter
{

    /**
     * Converts given profile model to profile entity.
     *
     * @param model {@link ProfileModel} to convert
     * @return {@link Profile}
     * @throws ModelToEntityConversionFailed
     */
    Profile toEntity(ProfileModel model)
            throws ModelToEntityConversionFailed;

    /**
     * Converts given profile models to profile entities.
     *
     * @param models list of {@link ProfileModel}s to convert
     * @return list of {@link Profile}s
     * @throws ModelToEntityConversionFailed
     */
    List<Profile> toEntities(List<ProfileModel> models)
            throws ModelToEntityConversionFailed;

    /**
     * Converts given profile entity to profile model.
     *
     * @param entity {@link Profile} to convert
     * @return {@link ProfileModel}
     * @throws ModelToEntityConversionFailed
     */
    ProfileModel toModel(Profile entity);

    /**
     * Converts given profile entities to profile models.
     *
     * @param entities list of {@link Profile}s to convert
     * @return list of {@link ProfileModel}s
     * @throws ModelToEntityConversionFailed
     */
    List<ProfileModel> toModels(List<Profile> entities);

}
