package com.powerhouse.entities;

import javax.persistence.*;
import java.util.List;

/**
 * ProfileModel is a collection of 12 {@link Fraction}s representing consumption distribution for a year that should
 * add up to 1.
 */
@Entity
@Table(name = "Profile")
public class Profile
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "profileId", nullable = false)
    private String id;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "ProfileFraction", joinColumns = @JoinColumn(name = "profileId"),
            inverseJoinColumns = @JoinColumn(name = "fractionId"))
    private List<Fraction> fractions;
    private String name;

    public Profile()
    {
    }

    private Profile(Builder builder)
    {
        id = builder.id;
        fractions = builder.fractions;
        name = builder.name;
    }

    public String getId()
    {
        return id;
    }

    public List<Fraction> getFractions()
    {
        return fractions;
    }

    public String getName()
    {
        return name;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public Builder builderFromCurrent()
    {
        return new Builder(this);
    }

    /**
     * Builder for {@link Fraction}.
     */
    public static class Builder
    {

        private String id;
        private List<Fraction> fractions;
        private String name;

        public Builder()
        {
        }

        public Builder(Profile profile)
        {
            this.id = profile.id;
            this.fractions = profile.fractions;
            this.name = profile.name;
        }

        public Profile build()
        {
            return new Profile(this);
        }

        public Builder withId(String id)
        {
            this.id = id;
            return this;
        }

        public Builder withFractions(List<Fraction> fractions)
        {
            this.fractions = fractions;
            return this;
        }

        public Builder withName(String name)
        {
            this.name = name;
            return this;
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Profile profile = (Profile) o;

        if (id != null ? !id.equals(profile.id) : profile.id != null) return false;
        if (fractions != null ? !fractions.equals(profile.fractions) : profile.fractions != null) return false;
        return name != null ? name.equals(profile.name) : profile.name == null;
    }

    @Override
    public int hashCode()
    {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (fractions != null ? fractions.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
