package com.powerhouse.entities;

import javax.persistence.*;

/**
 * Connection is installed at consumers premises to monitor consumption.
 */
@Entity
@Table(name = "Connection")
public class Connection
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "connectionId")
    private String id;
    private String name;

    public Connection()
    {
    }

    private Connection(Builder builder)
    {
        id = builder.id;
        name = builder.name;
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public Builder builderFromCurrent()
    {
        return new Builder(this);
    }

    /**
     * Builder for {@link Connection}.
     */
    public static class Builder
    {

        private String id;
        private String name;

        public Builder()
        {
        }

        public Builder(Connection connection)
        {
            this.id = connection.id;
            this.name = connection.name;
        }

        public Connection build()
        {
            return new Connection(this);
        }

        public Builder withId(String id)
        {
            this.id = id;
            return this;
        }

        public Builder withName(String name)
        {
            this.name = name;
            return this;
        }

    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Connection that = (Connection) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode()
    {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
