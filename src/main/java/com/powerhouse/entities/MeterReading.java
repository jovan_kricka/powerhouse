package com.powerhouse.entities;

import com.powerhouse.util.Month;

import javax.persistence.*;

/**
 * Meter reading represents number that the meter is showing at a specific date and time.
 */
@Entity
@Table(name = "MeterReading")
public class MeterReading
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "meterReadingId", nullable = false)
    private String id;
    @ManyToOne(optional = false)
    @JoinColumn(name = "connectionId", referencedColumnName = "connectionId")
    private Connection connection;
    @ManyToOne(optional = false)
    @JoinColumn(name = "profileId", referencedColumnName = "profileId")
    private Profile profile;
    @Column(name = "month")
    private Month month;
    @Column(name = "value")
    private double value;

    public MeterReading()
    {
    }

    private MeterReading(Builder builder)
    {
        id = builder.id;
        connection = builder.connection;
        profile = builder.profile;
        month = builder.month;
        value = builder.value;
    }

    public String getId()
    {
        return id;
    }

    public Connection getConnection()
    {
        return connection;
    }

    public Profile getProfile()
    {
        return profile;
    }

    public Month getMonth()
    {
        return month;
    }

    public double getValue()
    {
        return value;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public Builder builderFromCurrent()
    {
        return new Builder(this);
    }

    /**
     * Builder for {@link MeterReading}.
     */
    public static class Builder
    {

        private String id;
        private Connection connection;
        private Profile profile;
        private Month month;
        private double value;

        public Builder()
        {
        }

        public Builder(MeterReading meterReading)
        {
            this.id = meterReading.id;
            this.connection = meterReading.connection;
            this.profile = meterReading.profile;
            this.month = meterReading.month;
            this.value = meterReading.value;
        }

        public MeterReading build()
        {
            return new MeterReading(this);
        }

        public Builder withId(String id)
        {
            this.id = id;
            return this;
        }

        public Builder withConnection(Connection connection)
        {
            this.connection = connection;
            return this;
        }

        public Builder withProfile(Profile profile)
        {
            this.profile = profile;
            return this;
        }

        public Builder withMonth(Month month)
        {
            this.month = month;
            return this;
        }

        public Builder withValue(double value)
        {
            this.value = value;
            return this;
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MeterReading that = (MeterReading) o;

        if (Double.compare(that.value, value) != 0) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (connection != null ? !connection.equals(that.connection) : that.connection != null) return false;
        if (profile != null ? !profile.equals(that.profile) : that.profile != null) return false;
        return month == that.month;
    }

    @Override
    public int hashCode()
    {
        int result;
        long temp;
        result = id != null ? id.hashCode() : 0;
        result = 31 * result + (connection != null ? connection.hashCode() : 0);
        result = 31 * result + (profile != null ? profile.hashCode() : 0);
        result = 31 * result + (month != null ? month.hashCode() : 0);
        temp = Double.doubleToLongBits(value);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
