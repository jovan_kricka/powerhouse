package com.powerhouse.entities;

import com.powerhouse.util.Month;

import javax.persistence.*;

/**
 * FractionModel represents allowed/expected ratio between the consumption for the given month and the consumption of
 * the whole year.
 */
@Entity
@Table(name = "Fraction")
public class Fraction
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fractionId")
    private String id;
    @Column(name = "month")
    private Month month;
    @Column(name = "fraction")
    private float fraction;

    public Fraction()
    {
    }

    private Fraction(Builder builder)
    {
        id = builder.id;
        month = builder.month;
        fraction = builder.fraction;
    }

    public String getId()
    {
        return id;
    }

    public Month getMonth()
    {
        return month;
    }

    public float getFraction()
    {
        return fraction;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public Builder builderFromCurrent()
    {
        return new Builder(this);
    }

    /**
     * Builder for {@link Fraction}.
     */
    public static class Builder
    {

        private String id;
        private Month month;
        private float fraction;

        public Builder()
        {
        }

        public Builder(Fraction fraction)
        {
            this.id = fraction.id;
            this.month = fraction.month;
            this.fraction = fraction.fraction;
        }

        public Fraction build()
        {
            return new Fraction(this);
        }

        public Builder withId(String id)
        {
            this.id = id;
            return this;
        }

        public Builder withMonth(Month month)
        {
            this.month = month;
            return this;
        }

        public Builder withFraction(float fraction)
        {
            this.fraction = fraction;
            return this;
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Fraction fraction1 = (Fraction) o;

        if (Float.compare(fraction1.fraction, fraction) != 0) return false;
        if (id != null ? !id.equals(fraction1.id) : fraction1.id != null) return false;
        return month == fraction1.month;
    }

    @Override
    public int hashCode()
    {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (month != null ? month.hashCode() : 0);
        result = 31 * result + (fraction != +0.0f ? Float.floatToIntBits(fraction) : 0);
        return result;
    }
}
