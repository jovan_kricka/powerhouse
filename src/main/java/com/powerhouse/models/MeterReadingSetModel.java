package com.powerhouse.models;

import java.util.List;

/**
 * {@link MeterReadingSetModel} is a set of 12 {@link MeterReadingModel} coming from the client side.
 */
public class MeterReadingSetModel
{

    private List<MeterReadingModel> meterReadings;

    public MeterReadingSetModel()
    {
    }

    private MeterReadingSetModel(Builder builder)
    {
        meterReadings = builder.meterReadings;
    }

    public List<MeterReadingModel> getMeterReadings()
    {
        return meterReadings;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public Builder builderFromCurrent()
    {
        return new Builder(this);
    }

    /**
     * Builder for {@link MeterReadingSetModel}.
     */
    public static class Builder
    {

        private List<MeterReadingModel> meterReadings;

        public Builder()
        {
        }

        public Builder(MeterReadingSetModel meterReadingSet)
        {
            this.meterReadings = meterReadingSet.meterReadings;
        }

        public MeterReadingSetModel build()
        {
            return new MeterReadingSetModel(this);
        }

        public Builder withMeterReadings(List<MeterReadingModel> meterReadings)
        {
            this.meterReadings = meterReadings;
            return this;
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MeterReadingSetModel that = (MeterReadingSetModel) o;

        return meterReadings != null ? meterReadings.equals(that.meterReadings) : that.meterReadings == null;
    }

    @Override
    public int hashCode()
    {
        return meterReadings != null ? meterReadings.hashCode() : 0;
    }
}
