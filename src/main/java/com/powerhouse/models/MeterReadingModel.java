package com.powerhouse.models;

import com.powerhouse.entities.MeterReading;

/**
 * Model of {@link MeterReading} that will be coming from the client side.
 */
public class MeterReadingModel
{

    private String id;
    private String connectionId;
    private String profileName;
    private String month;
    private String value;

    public MeterReadingModel()
    {
    }

    private MeterReadingModel(Builder builder)
    {
        id = builder.id;
        connectionId = builder.connectionId;
        profileName = builder.profileName;
        month = builder.month;
        value = builder.value;
    }

    public String getId()
    {
        return id;
    }

    public String getConnectionId()
    {
        return connectionId;
    }

    public String getProfileName()
    {
        return profileName;
    }

    public String getMonth()
    {
        return month;
    }

    public String getValue()
    {
        return value;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public Builder builderFromCurrent()
    {
        return new Builder(this);
    }

    /**
     * Builder for {@link MeterReadingModel}.
     */
    public static class Builder
    {

        private String id;
        private String connectionId;
        private String profileName;
        private String month;
        private String value;

        public Builder()
        {
        }

        public Builder(MeterReadingModel meterReading)
        {
            this.id = meterReading.id;
            this.connectionId = meterReading.connectionId;
            this.profileName = meterReading.profileName;
            this.month = meterReading.month;
            this.value = meterReading.value;
        }

        public MeterReadingModel build()
        {
            return new MeterReadingModel(this);
        }

        public Builder withId(String id)
        {
            this.id = id;
            return this;
        }

        public Builder withConnectionId(String connectionId)
        {
            this.connectionId = connectionId;
            return this;
        }

        public Builder withProfileName(String profileName)
        {
            this.profileName = profileName;
            return this;
        }

        public Builder withMonth(String month)
        {
            this.month = month;
            return this;
        }

        public Builder withValue(String value)
        {
            this.value = value;
            return this;
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MeterReadingModel that = (MeterReadingModel) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (connectionId != null ? !connectionId.equals(that.connectionId) : that.connectionId != null) return false;
        if (profileName != null ? !profileName.equals(that.profileName) : that.profileName != null) return false;
        if (month != null ? !month.equals(that.month) : that.month != null) return false;
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode()
    {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (connectionId != null ? connectionId.hashCode() : 0);
        result = 31 * result + (profileName != null ? profileName.hashCode() : 0);
        result = 31 * result + (month != null ? month.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
