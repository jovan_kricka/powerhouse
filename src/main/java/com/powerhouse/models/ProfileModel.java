package com.powerhouse.models;

import com.powerhouse.entities.Profile;

import java.util.List;

/**
 * Model of {@link Profile} that will be coming from the client side.
 */
public class ProfileModel
{

    private String id;
    private String name;
    private List<FractionModel> fractions;

    public ProfileModel()
    {
    }

    private ProfileModel(Builder builder)
    {
        id = builder.id;
        fractions = builder.fractions;
        name = builder.name;
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public List<FractionModel> getFractions()
    {
        return fractions;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public Builder builderFromCurrent()
    {
        return new Builder(this);
    }

    /**
     * Builder for {@link FractionModel}.
     */
    public static class Builder
    {

        private String id;
        private List<FractionModel> fractions;
        private String name;

        public Builder()
        {
        }

        public Builder(ProfileModel profile)
        {
            this.id = profile.id;
            this.fractions = profile.fractions;
            this.name = profile.name;
        }

        public ProfileModel build()
        {
            return new ProfileModel(this);
        }

        public Builder withId(String id)
        {
            this.id = id;
            return this;
        }

        public Builder withFractions(List<FractionModel> fractions)
        {
            this.fractions = fractions;
            return this;
        }

        public Builder withName(String name)
        {
            this.name = name;
            return this;
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfileModel that = (ProfileModel) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return fractions != null ? fractions.equals(that.fractions) : that.fractions == null;
    }

    @Override
    public int hashCode()
    {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (fractions != null ? fractions.hashCode() : 0);
        return result;
    }
}
