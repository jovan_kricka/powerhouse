package com.powerhouse.models;

import com.powerhouse.entities.Fraction;

/**
 * Model of {@link Fraction} that will be coming from the client side.
 */
public class FractionModel
{

    private String id;
    private String month;
    private String value;

    public FractionModel()
    {
    }

    private FractionModel(Builder builder)
    {
        id = builder.id;
        month = builder.month;
        value = builder.value;
    }

    public String getId()
    {
        return id;
    }

    public String getMonth()
    {
        return month;
    }

    public String getValue()
    {
        return value;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public Builder builderFromCurrent()
    {
        return new Builder(this);
    }

    /**
     * Builder for {@link FractionModel}.
     */
    public static class Builder
    {

        private String id;
        private String month;
        private String value;

        public Builder()
        {
        }

        public Builder(FractionModel fraction)
        {
            this.id = fraction.id;
            this.month = fraction.month;
            this.value = fraction.value;
        }

        public FractionModel build()
        {
            return new FractionModel(this);
        }

        public Builder withId(String id)
        {
            this.id = id;
            return this;
        }

        public Builder withMonth(String month)
        {
            this.month = month;
            return this;
        }

        public Builder withValue(String fraction)
        {
            this.value = fraction;
            return this;
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FractionModel that = (FractionModel) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (month != null ? !month.equals(that.month) : that.month != null) return false;
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode()
    {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (month != null ? month.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
