package com.powerhouse.controllers;

import com.powerhouse.services.MeterReadingService;
import com.powerhouse.util.Month;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.text.DecimalFormat;

/**
 * REST controller for retrieving consumption of a given month.
 */
@RestController
@RequestMapping("/consumption")
public class ConsumptionController extends BaseController
{

    private static final String RELEVANT_METER_READINGS_NOT_FOUND = "Meter readings needed to calculate this " +
            "consumption are not found.";
    private static final String INVALID_MONTH_VALUE = "Invalid month value.";

    private final MeterReadingService meterReadingService;

    @Autowired
    public ConsumptionController(MeterReadingService meterReadingService)
    {
        this.meterReadingService = meterReadingService;
    }

    /**
     * Returns a consumption for a given month on a connection with a given id.
     *
     * @param connectionId that we are querying consumption for
     * @param month        that we are querying consumption for
     * @param response     http response
     * @return string representing consumption for a given month on a connection with a given id if there is one
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{connectionId}/{month}")
    public @ResponseBody
    String get(@PathVariable String connectionId, @PathVariable String month, HttpServletResponse response)
    {
        if (!Month.isValidMonth(month))
        {
            status(response, HttpStatus.BAD_REQUEST);
            return INVALID_MONTH_VALUE;
        }
        if (!meterReadingService.existsForConnection(connectionId))
        {
            status(response, HttpStatus.NOT_FOUND);
            return RELEVANT_METER_READINGS_NOT_FOUND;
        }
        Double consumption = meterReadingService.getConsumptionForConnectionAndMonth(connectionId, month);
        if (consumption == null)
        {
            status(response, HttpStatus.NOT_FOUND);
            return RELEVANT_METER_READINGS_NOT_FOUND;
        } else
        {
            status(response, HttpStatus.OK);
            return new DecimalFormat("#.##").format(consumption);
        }
    }

}
