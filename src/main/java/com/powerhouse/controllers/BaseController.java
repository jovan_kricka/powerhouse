package com.powerhouse.controllers;

import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;

/**
 * Base class containing common methods for app controllers.
 */
public class BaseController
{

    protected static final String PUT_FAILED_MESSAGE = "Validation or model to entity conversion failed. ";

    /**
     * Sets given {@link HttpStatus} on the given {@link HttpServletResponse} and returns reason phrase of the given
     * http status.
     *
     * @param response http response
     * @param status   http status to set
     * @return reason phrase of the http status
     */
    protected String status(HttpServletResponse response, HttpStatus status)
    {
        response.setStatus(status.value());
        return status.getReasonPhrase();
    }

    /**
     * Sets given {@link HttpStatus} on the given {@link HttpServletResponse} and returns given message.
     *
     * @param response http response
     * @param status   http status to set
     * @param message  message to return
     * @return message
     */
    protected String status(HttpServletResponse response, HttpStatus status, String message)
    {
        response.setStatus(status.value());
        return message;
    }

}
