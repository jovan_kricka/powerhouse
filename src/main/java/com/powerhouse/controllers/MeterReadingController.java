package com.powerhouse.controllers;

import com.powerhouse.entities.MeterReading;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.exceptions.ValidationFailedException;
import com.powerhouse.models.MeterReadingModel;
import com.powerhouse.services.MeterReadingService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for {@link MeterReading} endpoint.
 */
@RestController
@RequestMapping("/meterReading")
public class MeterReadingController extends BaseController
{

    private static final Logger logger = Logger.getLogger(MeterReadingController.class);

    private static final String UPDATING_NON_EXISTING_METER_READING = "Meter reading that you are trying to update " +
            "does not exist. If you are trying to create meter reading, you must do this by submitting array of 12 " +
            "meter readings for a connection.";

    private final MeterReadingService meterReadingService;

    @Autowired
    public MeterReadingController(MeterReadingService meterReadingService)
    {
        this.meterReadingService = meterReadingService;
    }

    /**
     * Returns all meter readings.
     *
     * @return list of {@link MeterReadingModel}s
     */
    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    List<MeterReadingModel> getAll()
    {
        return meterReadingService.getMeterReadings();
    }

    /**
     * Returns a single meter reading with the given id.
     *
     * @param id       of the meter reading to return
     * @param response http response
     * @return {@link MeterReadingModel} of the meter reading with the given id
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public @ResponseBody
    MeterReadingModel get(@PathVariable String id, HttpServletResponse response)
    {
        if (!meterReadingService.exists(id))
        {
            status(response, HttpStatus.NOT_FOUND);
            return null;
        }

        return meterReadingService.getMeterReading(id);
    }

    /**
     * Takes given {@link MeterReadingModel} from the client and either creates new meter reading or updates existing
     * one.
     *
     * @param meterReading model of the meter reading to create/update
     * @param response     http response
     * @return string containing either error message if creation/updating failed, or OK string
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String put(@RequestBody MeterReadingModel meterReading, HttpServletResponse response)
    {
        try
        {
            if (meterReadingService.exists(meterReading.getId()))
            {
                meterReadingService.saveMeterReading(meterReading);
                return status(response, HttpStatus.OK);
            } else
            {
                return status(response, HttpStatus.NOT_FOUND, UPDATING_NON_EXISTING_METER_READING);
            }
        } catch (ModelToEntityConversionFailed | ValidationFailedException ex)
        {
            logger.info(PUT_FAILED_MESSAGE + ex.getLocalizedMessage());
            return status(response, HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

}
