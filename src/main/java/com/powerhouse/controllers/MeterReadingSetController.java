package com.powerhouse.controllers;

import com.powerhouse.entities.MeterReading;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.exceptions.ValidationFailedException;
import com.powerhouse.models.MeterReadingSetModel;
import com.powerhouse.services.MeterReadingService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * REST controller for set of {@link MeterReading}s.
 */
@RestController
@RequestMapping("/meterReadingSet")
public class MeterReadingSetController extends BaseController
{

    private static final Logger logger = Logger.getLogger(MeterReadingSetController.class);

    private final MeterReadingService meterReadingService;

    @Autowired
    public MeterReadingSetController(MeterReadingService meterReadingService)
    {
        this.meterReadingService = meterReadingService;
    }

    /**
     * Takes given {@link MeterReadingSetModel} from the client and either creates/updates meter readings in it.
     *
     * @param meterReadingSet model of the meter reading set to create/update
     * @param response        http response
     * @return string containing either error message if creation/updating failed, or OK string
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String put(@RequestBody MeterReadingSetModel meterReadingSet, HttpServletResponse response)
    {
        try
        {
            meterReadingService.saveMeterReadingSet(meterReadingSet);
            return status(response, HttpStatus.OK);
        } catch (ModelToEntityConversionFailed | ValidationFailedException ex)
        {
            logger.info(PUT_FAILED_MESSAGE + ex.getLocalizedMessage());
            return status(response, HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    /**
     * Deletes all meter readings for a connection with a given id.
     *
     * @param connectionId id of the connection
     * @param response     http response
     * @return reason phrase of the http status
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/{connectionId}")
    public @ResponseBody
    String delete(@PathVariable String connectionId, HttpServletResponse response)
    {
        if (!meterReadingService.existsForConnection(connectionId))
        {
            return status(response, HttpStatus.NOT_FOUND);
        }

        meterReadingService.removeMeterReadingsForConnection(connectionId);
        return status(response, HttpStatus.OK);
    }

}
