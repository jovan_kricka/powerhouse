package com.powerhouse.controllers;

import com.powerhouse.entities.Connection;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.models.ConnectionModel;
import com.powerhouse.services.ConnectionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for {@link Connection}s.
 */
@RestController
@RequestMapping("/connection")
public class ConnectionController extends BaseController
{

    private static final Logger logger = Logger.getLogger(ConnectionController.class);

    private final ConnectionService connectionService;

    @Autowired
    public ConnectionController(ConnectionService connectionService)
    {
        this.connectionService = connectionService;
    }

    /**
     * Returns all connections.
     *
     * @return list of {@link ConnectionModel}s
     */
    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    List<ConnectionModel> getAll()
    {
        return connectionService.getConnections();
    }

    /**
     * Returns a single connection with the given id.
     *
     * @param id       of the connection to return
     * @param response http response
     * @return {@link ConnectionModel} of the connection with the given id
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public @ResponseBody
    ConnectionModel get(@PathVariable String id, HttpServletResponse response)
    {
        if (!connectionService.exists(id))
        {
            logger.info("Connection with id " + id + " was not found.");
            status(response, HttpStatus.NOT_FOUND);
            return null;
        }

        return connectionService.getConnection(id);
    }

    /**
     * Takes given {@link ConnectionModel} from the client and either creates new connection or updates existing one.
     *
     * @param connection model of the connection to create/update
     * @param response   http response
     * @return string containing either error message if creation/updating failed, or OK string
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String put(@RequestBody ConnectionModel connection, HttpServletResponse response)
    {
        try
        {
            if (connectionService.exists(connection.getId()))
            {
                String id = connectionService.saveConnection(connection);
                return status(response, HttpStatus.OK, id);
            } else
            {
                String id = connectionService.saveConnection(connection);
                return status(response, HttpStatus.CREATED, id);
            }
        } catch (ModelToEntityConversionFailed ex)
        {
            logger.info(PUT_FAILED_MESSAGE + ex.getLocalizedMessage());
            return status(response, HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    /**
     * Deletes connection with the given id if it exists.
     *
     * @param id       of the connection to delete
     * @param response http response
     * @return reason phrase of the http status
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public @ResponseBody
    String delete(@PathVariable String id, HttpServletResponse response)
    {
        if (!connectionService.exists(id))
        {
            logger.info("Connection with id " + id + " was not found.");
            return status(response, HttpStatus.NOT_FOUND);
        }

        connectionService.removeConnection(id);
        return status(response, HttpStatus.OK);
    }

}
