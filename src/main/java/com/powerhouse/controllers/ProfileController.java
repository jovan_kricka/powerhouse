package com.powerhouse.controllers;

import com.powerhouse.entities.Profile;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.exceptions.ValidationFailedException;
import com.powerhouse.models.ProfileModel;
import com.powerhouse.services.ProfileService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for {@link Profile}s
 */
@RestController
@RequestMapping("/profile")
public class ProfileController extends BaseController
{

    private static final Logger logger = Logger.getLogger(ProfileController.class);

    private final ProfileService profileService;

    @Autowired
    public ProfileController(ProfileService profileService)
    {
        this.profileService = profileService;
    }

    /**
     * Returns all profiles.
     *
     * @return list of {@link ProfileModel}s
     */
    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    List<ProfileModel> getAll()
    {
        return profileService.getProfiles();
    }

    /**
     * Returns a single profile with the given id.
     *
     * @param id       of the profile to return
     * @param response http response
     * @return {@link ProfileModel} of the profile with the given id
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public @ResponseBody
    ProfileModel get(@PathVariable String id, HttpServletResponse response)
    {
        if (!profileService.exists(id))
        {
            status(response, HttpStatus.NOT_FOUND);
            return null;
        }

        return profileService.getProfile(id);
    }

    /**
     * Takes given {@link ProfileModel} from the client and either creates new profile or updates existing one.
     *
     * @param profile  model of the profile to create/update
     * @param response http response
     * @return string containing either error message if creation/updating failed, or OK string
     */
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    String put(@RequestBody ProfileModel profile, HttpServletResponse response)
    {
        try
        {
            if (profileService.exists(profile.getId()))
            {
                String id = profileService.saveProfile(profile);
                return status(response, HttpStatus.OK, id);
            } else
            {
                String id = profileService.saveProfile(profile);
                return status(response, HttpStatus.CREATED, id);
            }
        } catch (ModelToEntityConversionFailed | ValidationFailedException ex)
        {
            logger.info(PUT_FAILED_MESSAGE + ex.getLocalizedMessage());
            return status(response, HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    /**
     * Deletes profile with the given id if it exists.
     *
     * @param id       of the profile to delete
     * @param response http response
     * @return reason phrase of the http status
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public @ResponseBody
    String delete(@PathVariable String id, HttpServletResponse response)
    {
        if (!profileService.exists(id))
        {
            return status(response, HttpStatus.NOT_FOUND);
        }

        profileService.removeProfile(id);
        return status(response, HttpStatus.OK);
    }

}
