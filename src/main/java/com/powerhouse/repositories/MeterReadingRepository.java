package com.powerhouse.repositories;

import com.powerhouse.entities.Connection;
import com.powerhouse.entities.MeterReading;
import com.powerhouse.util.Month;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * JPA repository for {@link MeterReading}
 */
@Repository
public interface MeterReadingRepository extends CrudRepository<MeterReading, String>
{

    List<MeterReading> findAllByOrderByMonthAsc();

    List<MeterReading> findByConnectionOrderByMonthAsc(Connection connection);

    MeterReading findByConnectionAndMonth(Connection connection, Month month);

    @Transactional
    void deleteByConnection(Connection connection);

}
