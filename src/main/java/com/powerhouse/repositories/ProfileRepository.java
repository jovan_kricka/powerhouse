package com.powerhouse.repositories;

import com.powerhouse.entities.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * JPA repository for {@link Profile}.
 */
@Repository
public interface ProfileRepository extends CrudRepository<Profile, String>
{

    Profile findByName(String name);

}
