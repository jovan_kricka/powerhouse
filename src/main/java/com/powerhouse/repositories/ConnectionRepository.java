package com.powerhouse.repositories;

import com.powerhouse.entities.Connection;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * JPA Repository for {@link Connection}s
 */
@Repository
public interface ConnectionRepository extends CrudRepository<Connection, String>
{
}
