package com.powerhouse.services;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.powerhouse.adapters.ProfileAdapter;
import com.powerhouse.entities.Profile;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.exceptions.ValidationFailedException;
import com.powerhouse.models.ProfileModel;
import com.powerhouse.repositories.ProfileRepository;
import com.powerhouse.validators.ProfileValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileServiceImpl implements ProfileService
{

    private final ProfileRepository repository;
    private final ProfileAdapter adapter;
    private final ProfileValidator validator;

    @Autowired
    public ProfileServiceImpl(ProfileRepository profileRepository, ProfileAdapter adapter,
                              ProfileValidator validator)
    {
        this.repository = profileRepository;
        this.adapter = adapter;
        this.validator = validator;
    }

    @Override
    public List<ProfileModel> getProfiles()
    {
        List<Profile> profiles = Lists.newArrayList(repository.findAll());
        return adapter.toModels(profiles);
    }

    @Override
    public ProfileModel getProfile(String id)
    {
        Profile profile = repository.findOne(id);
        return adapter.toModel(profile);
    }

    @Override
    public String saveProfile(ProfileModel model)
            throws ModelToEntityConversionFailed, ValidationFailedException
    {
        Profile entity = adapter.toEntity(model);
        String error = validator.validate(entity);
        if (Strings.isNullOrEmpty(error))
        {
            return repository.save(entity).getId();
        } else
        {
            throw new ValidationFailedException(error);
        }
    }

    @Override
    public void removeProfile(String id)
    {
        repository.delete(id);
    }

    @Override
    public boolean exists(String id)
    {
        return Strings.isNullOrEmpty(id) ? false : repository.exists(id);
    }
}
