package com.powerhouse.services;

import com.powerhouse.entities.MeterReading;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.exceptions.ValidationFailedException;
import com.powerhouse.models.MeterReadingModel;
import com.powerhouse.models.MeterReadingSetModel;

import java.util.List;

/**
 * Provides api for operations on {@link MeterReading}.
 */
public interface MeterReadingService
{

    /**
     * Gets all meter readings.
     *
     * @return list of {@link MeterReadingModel}s
     */
    List<MeterReadingModel> getMeterReadings();

    /**
     * Gets meter reading with given id.
     *
     * @param id of the meter reading to fetch
     * @return {@link MeterReadingModel}
     */
    MeterReadingModel getMeterReading(String id);

    /**
     * Gets consumption for a connection with a given id on a given month.
     *
     * @param connectionId
     * @param month
     * @return double representing consumption on a given connection for a given month
     */
    Double getConsumptionForConnectionAndMonth(String connectionId, String month);

    /**
     * Creates/updates meter reading based on given {@link MeterReadingModel}.
     *
     * @param connection model of the meter reading to create
     * @return id of the saved meter reading
     * @throws ModelToEntityConversionFailed
     * @throws ValidationFailedException
     */
    String saveMeterReading(MeterReadingModel connection)
            throws ModelToEntityConversionFailed, ValidationFailedException;


    /**
     * Creates/updates all meter readings from a given {@link MeterReadingSetModel}.
     *
     * @param meterReadingSetModel set of meter readings to create/update
     * @throws ModelToEntityConversionFailed
     * @throws ValidationFailedException
     */
    void saveMeterReadingSet(MeterReadingSetModel meterReadingSetModel)
            throws ModelToEntityConversionFailed, ValidationFailedException;

    /**
     * Deletes meter reading with the given id.
     *
     * @param id of the meter reading to delete
     */
    void removeMeterReading(String id);

    /**
     * Deletes meter readings for a connection with given id.
     *
     * @param connectionId of the connection
     */
    void removeMeterReadingsForConnection(String connectionId);

    /**
     * Checks if meter reading with given id exists.
     *
     * @param id of the meter reading to check
     * @return true if meter reading exists, false otherwise
     */
    boolean exists(String id);

    /**
     * Checks if meter readings exist for a connection with a given id.
     *
     * @param connectionId of the connection to check
     * @return true if meter readings exist for a connection with given id, false otherwise
     */
    boolean existsForConnection(String connectionId);
}
