package com.powerhouse.services;

import com.powerhouse.exceptions.FileDropWatcherServiceMalfunctionedException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Watches current directory for dropped input files, and when new file is dropped it triggers its processing.
 */
@Service
public class FileDropWatcherService implements Runnable
{

    private static final Logger logger = Logger.getLogger(FileDropWatcherService.class);

    private final ProfileService profileService;
    private final MeterReadingService meterReadingService;

    @Autowired
    public FileDropWatcherService(ProfileService profileService, MeterReadingService meterReadingService)
    {
        this.profileService = profileService;
        this.meterReadingService = meterReadingService;

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(this);
    }

    @Override
    public void run()
    {
        WatchService watchService;
        try
        {
            watchService = FileSystems.getDefault().newWatchService();
            Path path = Paths.get("." + File.separator);
            logger.info("Watching for " + path.toAbsolutePath().toString());

            path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);

            WatchKey key;
            while ((key = watchService.take()) != null)
            {
                for (WatchEvent<?> event : key.pollEvents())
                {
                    triggerFileProcessingService(Paths.get("." + File.separator + event.context()));
                }
                key.reset();
            }
        } catch (IOException | InterruptedException ex)
        {
            logger.error("Error occurred in file drop watching service. " + ex.getLocalizedMessage());
            throw new FileDropWatcherServiceMalfunctionedException(ex);
        }
    }

    /**
     * Creates a new thread of {@link FileProcessingService} that will process input file on a given {@link Path}.
     *
     * @param filePath of the input file
     */
    private void triggerFileProcessingService(Path filePath)
    {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        FileProcessingService fileProcessingService = new FileProcessingService(filePath, profileService,
                meterReadingService);
        executorService.execute(fileProcessingService);
    }


}
