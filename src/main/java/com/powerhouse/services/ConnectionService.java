package com.powerhouse.services;

import com.powerhouse.entities.Connection;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.exceptions.ValidationFailedException;
import com.powerhouse.models.ConnectionModel;

import java.util.List;

/**
 * Provides api for operations on {@link Connection}.
 */
public interface ConnectionService
{

    /**
     * Gets all connections.
     *
     * @return list of {@link ConnectionModel}s
     */
    List<ConnectionModel> getConnections();

    /**
     * Gets connection with given id.
     *
     * @param id of the connection to fetch
     * @return {@link ConnectionModel}
     */
    ConnectionModel getConnection(String id);

    /**
     * Creates connection based on given {@link ConnectionModel}.
     *
     * @param connection model of the connection to create
     * @return string representing the id of saved connection
     * @throws ModelToEntityConversionFailed
     * @throws ValidationFailedException
     */
    String saveConnection(ConnectionModel connection)
            throws ModelToEntityConversionFailed;

    /**
     * Deletes connection with the given id.
     *
     * @param id of the connection to delete
     */
    void removeConnection(String id);

    /**
     * Checks if connection with given id exists.
     *
     * @param id of the connection to check
     * @return true if connection exists, false otherwise
     */
    boolean exists(String id);

}
