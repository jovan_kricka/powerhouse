package com.powerhouse.services;

import com.powerhouse.entities.Profile;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.exceptions.ValidationFailedException;
import com.powerhouse.models.ProfileModel;

import java.util.List;

/**
 * Provides api for operations on {@link Profile}s.
 */
public interface ProfileService
{

    /**
     * Gets all profiles.
     *
     * @return list of {@link ProfileModel}s
     */
    List<ProfileModel> getProfiles();

    /**
     * Gets profile with given id.
     *
     * @param id of the profile to fetch
     * @return {@link ProfileModel}
     */
    ProfileModel getProfile(String id);

    /**
     * Creates profile based on given {@link ProfileModel}.
     *
     * @param profile model of the profile to create
     * @return id of the saved profile
     * @throws ModelToEntityConversionFailed
     * @throws ValidationFailedException
     */
    String saveProfile(ProfileModel profile)
            throws ModelToEntityConversionFailed, ValidationFailedException;

    /**
     * Deletes profile with the given id.
     *
     * @param id of the profile to delete
     */
    void removeProfile(String id);

    /**
     * Checks if profile with given id exists.
     *
     * @param id of the profile to check
     * @return true if profile exists, false otherwise
     */
    boolean exists(String id);

}
