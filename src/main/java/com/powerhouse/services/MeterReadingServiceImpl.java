package com.powerhouse.services;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.powerhouse.adapters.MeterReadingAdapter;
import com.powerhouse.entities.Connection;
import com.powerhouse.entities.MeterReading;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.exceptions.ValidationFailedException;
import com.powerhouse.models.MeterReadingModel;
import com.powerhouse.models.MeterReadingSetModel;
import com.powerhouse.repositories.ConnectionRepository;
import com.powerhouse.repositories.MeterReadingRepository;
import com.powerhouse.util.Month;
import com.powerhouse.validators.MeterReadingValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MeterReadingServiceImpl implements MeterReadingService
{

    private static final Logger logger = Logger.getLogger(MeterReadingServiceImpl.class);

    private final MeterReadingRepository repository;
    private final ConnectionRepository connectionRepository;
    private final MeterReadingAdapter adapter;
    private final MeterReadingValidator validator;

    @Autowired
    public MeterReadingServiceImpl(MeterReadingRepository profileRepository, MeterReadingAdapter adapter,
                                   MeterReadingValidator validator, ConnectionRepository connectionRepository)
    {
        this.repository = profileRepository;
        this.adapter = adapter;
        this.validator = validator;
        this.connectionRepository = connectionRepository;
    }

    @Override
    public List<MeterReadingModel> getMeterReadings()
    {
        List<MeterReading> profiles = Lists.newArrayList(repository.findAllByOrderByMonthAsc());
        return adapter.toModels(profiles);
    }

    @Override
    public MeterReadingModel getMeterReading(String id)
    {
        MeterReading meterReading = repository.findOne(id);
        return adapter.toModel(meterReading);
    }

    @Override
    public Double getConsumptionForConnectionAndMonth(String connectionId, String monthString)
    {
        try
        {
            Connection connection = connectionRepository.findOne(connectionId);
            Month month = Month.valueOf(monthString);
            MeterReading meterReading = repository.findByConnectionAndMonth(connection, month);
            if (Month.JAN.equals(month))
            {
                return meterReading.getValue();
            } else
            {
                MeterReading previousMeterReading = repository.findByConnectionAndMonth(connection, month.previous());
                if (previousMeterReading == null)
                {
                    return null;
                }
                return meterReading.getValue() - previousMeterReading.getValue();
            }
        } catch (IllegalArgumentException ex)
        {
            logger.info("Fetching consumption for connection and month failed. " + ex.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public String saveMeterReading(MeterReadingModel meterReading)
            throws ModelToEntityConversionFailed, ValidationFailedException
    {
        MeterReading entity = adapter.toEntity(meterReading);
        List<MeterReading> relatedReadings = repository.findByConnectionOrderByMonthAsc(entity.getConnection());
        String error = validator.validate(entity, relatedReadings);
        if (Strings.isNullOrEmpty(error))
        {
            return repository.save(entity).getId();
        } else
        {
            throw new ValidationFailedException(error);
        }
    }

    @Override
    public void saveMeterReadingSet(MeterReadingSetModel meterReadingSetModel)
            throws ModelToEntityConversionFailed, ValidationFailedException
    {
        List<MeterReading> meterReadings = adapter.toEntities(meterReadingSetModel.getMeterReadings());
        String error = validator.validate(meterReadings);
        if (Strings.isNullOrEmpty(error))
        {
            repository.save(meterReadings);
        } else
        {
            throw new ValidationFailedException(error);
        }
    }

    @Override
    public void removeMeterReading(String id)
    {
        repository.delete(id);
    }

    @Override
    public void removeMeterReadingsForConnection(String connectionId)
    {
        Connection connection = connectionRepository.findOne(connectionId);
        repository.deleteByConnection(connection);
    }

    @Override
    public boolean exists(String id)
    {
        return Strings.isNullOrEmpty(id) ? false : repository.exists(id);
    }

    @Override
    public boolean existsForConnection(String connectionId)
    {
        Connection connection = connectionRepository.findOne(connectionId);
        return !repository.findByConnectionOrderByMonthAsc(connection).isEmpty();
    }

}
