package com.powerhouse.services;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.powerhouse.adapters.ConnectionAdapter;
import com.powerhouse.entities.Connection;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.models.ConnectionModel;
import com.powerhouse.repositories.ConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConnectionServiceImpl implements ConnectionService
{

    private final ConnectionRepository repository;
    private final ConnectionAdapter adapter;

    @Autowired
    public ConnectionServiceImpl(ConnectionRepository repository, ConnectionAdapter adapter)
    {
        this.repository = repository;
        this.adapter = adapter;
    }

    @Override
    public List<ConnectionModel> getConnections()
    {
        List<Connection> connections = Lists.newArrayList(repository.findAll());
        return adapter.toModels(connections);
    }

    @Override
    public ConnectionModel getConnection(String id)
    {
        Connection connection = repository.findOne(id);
        return adapter.toModel(connection);
    }

    @Override
    public String saveConnection(ConnectionModel connection)
            throws ModelToEntityConversionFailed
    {
        Connection entity = adapter.toEntity(connection);
        return repository.save(entity).getId();
    }

    @Override
    public void removeConnection(String id)
    {
        repository.delete(id);
    }

    @Override
    public boolean exists(String id)
    {
        return Strings.isNullOrEmpty(id) ? false : repository.exists(id);
    }
}
