package com.powerhouse.services;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.powerhouse.exceptions.ModelToEntityConversionFailed;
import com.powerhouse.exceptions.ValidationFailedException;
import com.powerhouse.models.FractionModel;
import com.powerhouse.models.MeterReadingModel;
import com.powerhouse.models.MeterReadingSetModel;
import com.powerhouse.models.ProfileModel;
import com.powerhouse.util.Month;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Takes input file and processes it by storing data from it. This service is not managed by Spring since it must be
 * instantiated on demand by {@link FileDropWatcherService}.
 */
public class FileProcessingService implements Runnable
{
    private static final Logger logger = Logger.getLogger(FileProcessingService.class);

    private final ProfileService profileService;
    private final MeterReadingService meterReadingService;

    private final Path inputFilePath;
    private final Map<String, List<FractionModel>> fractionsPerProfiles;
    private final Map<String, List<MeterReadingModel>> meterReadingsPerConnections;

    public FileProcessingService(Path inputFilePath, ProfileService profileService,
                                 MeterReadingService meterReadingService)
    {
        this.inputFilePath = inputFilePath;
        this.profileService = profileService;
        this.meterReadingService = meterReadingService;
        this.fractionsPerProfiles = Maps.newHashMap();
        this.meterReadingsPerConnections = Maps.newHashMap();
    }

    @Override
    public void run()
    {
        processInputFile();
    }

    /**
     * Determines which type of input file we have, processes it accordingly, and then deletes it.
     */
    private void processInputFile()
    {
        try
        {
            waitForFileToBeAvailable();
            if (isProfilesAndFractionsCsv(inputFilePath))
            {
                processProfilesAndFractionsCsv(inputFilePath);
                inputFilePath.toFile().delete();
            } else if (isMeterReadingsCsv(inputFilePath))
            {
                processMeterReadingsCsv(inputFilePath);
                inputFilePath.toFile().delete();
            }
            logger.info("File " + inputFilePath.toString() + " processed successfully.");
        } catch (IOException ex)
        {
            logger.error("File processing failed. " + ex.getLocalizedMessage());
        }
    }

    /**
     * Waits for file to be available.
     */
    private void waitForFileToBeAvailable()
    {
        File inputFile = inputFilePath.toFile();
        while (!inputFile.renameTo(inputFile))
        {
            try
            {
                Thread.sleep(10);
            } catch (InterruptedException ex)
            {
                logger.error("Interrupted while waiting for file to become available. " + ex.getLocalizedMessage());
            }
        }
    }

    /**
     * Opens input file, reads meter readings from it, builds meter readings map, and then creates each meter reading
     * in that map.
     *
     * @param path of the input csv file
     * @throws IOException
     */
    private void processMeterReadingsCsv(Path path)
    {
        try (Stream<String> stream = Files.lines(path))
        {
            stream.forEach(this::processMeterReadingsCsvLine);
            for (Map.Entry<String, List<MeterReadingModel>> entry : meterReadingsPerConnections.entrySet())
            {
                MeterReadingSetModel meterReadingSetModel = MeterReadingSetModel.builder()
                        .withMeterReadings(entry.getValue())
                        .build();
                try
                {
                    meterReadingService.saveMeterReadingSet(meterReadingSetModel);
                } catch (ModelToEntityConversionFailed | ValidationFailedException ex)
                {
                    logger.info("File processing failed. " + ex.getLocalizedMessage());
                }
            }
        } catch (IOException ex)
        {
            logger.error("File processing failed. " + ex.getLocalizedMessage());
        }

    }

    /**
     * Processes one line of meter readings csv file, creates {@link MeterReadingModel} from it and stores it in
     * a global map.
     *
     * @param line of input csv file
     */
    private void processMeterReadingsCsvLine(String line)
    {
        if (!isValidMeterReadingCsvLine(line))
        {
            logger.info("Invalid meter reading csv line \"" + line + "\".");
            return;
        }

        String[] tokens = line.split(",");

        MeterReadingModel meterReadingModel = MeterReadingModel.builder()
                .withConnectionId(tokens[0])
                .withProfileName(tokens[1])
                .withMonth(tokens[2])
                .withValue(tokens[3])
                .build();

        String connectionId = tokens[0];
        List<MeterReadingModel> meterReadings = meterReadingsPerConnections.get(connectionId) == null ?
                Lists.newArrayList() : meterReadingsPerConnections.get(connectionId);
        meterReadings.add(meterReadingModel);
        meterReadingsPerConnections.put(connectionId, meterReadings);
    }

    /**
     * Opens input file, reads profile fractions from it, builds profile fractions map, and then creates each profile
     * in that map.
     *
     * @param path of the input csv file
     * @throws IOException
     */
    private void processProfilesAndFractionsCsv(Path path)
            throws IOException
    {
        try (Stream<String> stream = Files.lines(path))
        {
            stream.forEach(this::processProfileAndFractionsCsvLine);
            for (Map.Entry<String, List<FractionModel>> entry : fractionsPerProfiles.entrySet())
            {
                ProfileModel profileModel = ProfileModel.builder()
                        .withName(entry.getKey())
                        .withFractions(entry.getValue())
                        .build();
                try
                {
                    profileService.saveProfile(profileModel);
                } catch (ModelToEntityConversionFailed | ValidationFailedException ex)
                {
                    logger.info("File processing failed. " + ex.getLocalizedMessage());
                }
            }
        } catch (IOException ex)
        {
            logger.error("File processing failed. " + ex.getLocalizedMessage());
        }
    }

    /**
     * Processes one line of profiles and fractions csv file, creates {@link FractionModel} from it and stores it in
     * a global map.
     *
     * @param line of input csv file
     */
    private void processProfileAndFractionsCsvLine(String line)
    {
        if (!isValidProfilesAndFractionsCsvLine(line))
        {
            logger.info("Invalid profiles and fractions csv line \"" + line + "\".");
            return;
        }
        String[] tokens = line.split(",");

        FractionModel fractionModel = FractionModel.builder()
                .withMonth(tokens[0])
                .withValue(tokens[2])
                .build();

        String profileName = tokens[1];
        List<FractionModel> profileFractions = fractionsPerProfiles.get(profileName) == null ?
                Lists.newArrayList() : fractionsPerProfiles.get(profileName);
        profileFractions.add(fractionModel);
        fractionsPerProfiles.put(profileName, profileFractions);
    }

    /**
     * Checks if first line of a file with given path can be parsed as a meter reading.
     *
     * @param path of the input file
     * @return true if the first line of input file is in the correct format, false otherwise
     * @throws IOException
     */
    private boolean isMeterReadingsCsv(Path path)
            throws IOException
    {
        String firstCsvLine = getFirstLineFromFile(path);
        return isValidMeterReadingCsvLine(firstCsvLine);
    }

    /**
     * Checks if the given string is a valid meter reading csv line.
     *
     * @param firstCsvLine string representing csv line
     * @return true if given string is a valid meter reading csv line, false otherwise
     */
    private boolean isValidMeterReadingCsvLine(String firstCsvLine)
    {
        if (Strings.isNullOrEmpty(firstCsvLine))
        {
            return false;
        }
        String[] tokens = firstCsvLine.split(",");

        if (tokens.length != 4)
        {
            return false;
        }

        try
        {
            Month.valueOf(tokens[2]);
            Double.parseDouble(tokens[3]);
            return true;
        } catch (IllegalArgumentException ex)
        {
            logger.info("Processing of the first line of file failed. " + ex.getLocalizedMessage());
            return false;
        }
    }

    /**
     * Checks if first line of a file on a given path can be parsed as a fraction of a profile.
     *
     * @param path of the input file
     * @return true if first line of the file is in the correct format, false otherwise
     * @throws IOException
     */
    private boolean isProfilesAndFractionsCsv(Path path)
            throws IOException
    {
        String firstCsvLine = getFirstLineFromFile(path);
        return isValidProfilesAndFractionsCsvLine(firstCsvLine);
    }

    /**
     * Checks if the given string is a valid profiles and fractions csv line.
     *
     * @param firstCsvLine string representing csv line
     * @return true if given string is a valid profiles and fractions csv line, false otherwise
     */
    private boolean isValidProfilesAndFractionsCsvLine(String firstCsvLine)
    {
        if (Strings.isNullOrEmpty(firstCsvLine))
        {
            return false;
        }
        String[] tokens = firstCsvLine.split(",");

        if (tokens.length != 3)
        {
            return false;
        }

        try
        {
            Month.valueOf(tokens[0]);
            Float.parseFloat(tokens[2]);
            return true;
        } catch (IllegalArgumentException ex)
        {
            logger.info("Processing of the first line of file failed. " + ex.getLocalizedMessage());
            return false;
        }
    }

    /**
     * Gets first line of text from a file on a given {@link Path}.
     *
     * @param path of a file
     * @return first line from the file
     * @throws IOException
     */
    private String getFirstLineFromFile(Path path)
            throws IOException
    {
        File inputFile = path.toFile();
        BufferedReader stream = new BufferedReader(new FileReader(inputFile));
        String firstLine = stream.readLine();
        stream.close();
        return firstLine;
    }
}
